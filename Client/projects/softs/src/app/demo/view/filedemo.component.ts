import {Component} from '@angular/core';
import {MessageService} from 'primeng/api';
import {BreadcrumbService} from '../../core/services/breadcrumb.service';

@Component({
    templateUrl: './filedemo.component.html',
    providers: [MessageService]
})
export class FileDemoComponent {

    uploadedFiles: any[] = [];

    constructor(private messageService: MessageService, private breadcrumbService: BreadcrumbService) {
        this.breadcrumbService.setItems([
            {label: 'UI Kit'},
            {label: 'File'}
        ]);
    }

    onUpload(event) {
        console.log(event)
        for (const file of event.files) {
            this.uploadedFiles.push(file);
        }
        console.log(this.uploadedFiles);
        this.messageService.add({severity: 'info', summary: 'Success', detail: 'File Uploaded'});
    }

    onBasicUpload(event) {
        this.messageService.add({severity: 'info', summary: 'Success', detail: 'File Uploaded with Basic Mode'});
    }
}
