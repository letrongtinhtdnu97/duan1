import { Component, OnInit } from '@angular/core';
import { MainComponent } from '../../../core/components/main/main.component';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  model: any[];

  constructor(public app: MainComponent) { }

  ngOnInit(): void {
    this.model = [
      {label: 'Dardboard', icon: 'pi pi-fw pi-home', routerLink: ['/']},
      {
          label: 'Quản lý tài khoản', icon: 'pi pi-fw pi-star-o', routerLink: ['/user'],
          items: [
              {label: 'Danh sách tài khoản', icon: 'pi pi-fw pi-id-card', routerLink: ['/user/list']},
              
          ]
      },
      {
        label: 'Khóa học', icon: 'pi pi-fw pi-star-o', routerLink: ['/course'],
        items: [
            {label: 'Môn học', icon: 'pi pi-fw pi-id-card', routerLink: ['/course/subject']},
            {label: 'Điểm môn học', icon: 'pi pi-fw pi-id-card', routerLink: ['/course/list']},
        ]
    },
  ]
  }

  onMenuClick(): void {
    this.app.menuClick = true;
  }
}
