import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { MainComponent } from './core/components/main/main.component';
import { LoginComponent } from './core/components/login/login.component';
import { AccessdeniedComponent } from './core/pages/accessdenied/accessdenied.component';
import { NotfoundComponent } from './core/pages/notfound/notfound.component';
import { ErrorComponent } from './core/pages/error/error.component';
import { UserComponent } from './student/components/user/user.component';
import { PointComponent } from './student/components/point/point.component';
import { CourseComponent } from './student/components/course/course.component';
import { UserAddComponent } from './student/components/user-add/user-add.component';
import { UserEditComponent } from './student/components/user-edit/user-edit.component';
import { SubjectComponent } from './student/components/subject/subject.component';
import { DashboardComponent } from './student/components/dashboard/dashboard.component';


@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: '', component: MainComponent,
                children: [
                    { path: '', component: DashboardComponent },
                    { path: 'user/list', component: UserComponent },
                    { path: 'user/add', component: UserAddComponent },
                    { path: 'user/edit', component: UserEditComponent },
                    { path: 'admin/point', component: PointComponent },
                    { path: 'course/list', component: CourseComponent },
                    { path: 'course/subject', component: SubjectComponent },
                    
                    
                ]
            },
            { path: 'login', component: LoginComponent },
            { path: 'error', component: ErrorComponent },
            { path: 'access', component: AccessdeniedComponent },
            { path: 'notfound', component: NotfoundComponent },
            { path: '**', redirectTo: '/notfound' },
        ], { scrollPositionRestoration: 'enabled', useHash: true })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
