export interface PermissionCategory {
    perCatId?: number;
    perCatCode?: string;
    perCatName?: string;
}

export class Permission {
    perId?: number;
    APP_ID?: number;
    PER_CODE?: string;
    PER_NAME?: string;
    PER_CAT_ID?: number;
    PER_CAT_NAME?: string;
    PER_CAT_CODE?: string;
    ACTIVE?: number;
    CREATED_ON?: any;
    CREATED_BY?: string;
    MODIFIED_ON?: any;
    MODIFIED_BY?: string;
}
