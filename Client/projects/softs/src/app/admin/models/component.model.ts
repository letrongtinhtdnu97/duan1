export class SComponent {
    COMPONENT_ID?: number;
    COMPONENT_NAME?: string;
    DESCRIPTION?: string;
    APP_ID?: number;
    APP_NAME?: string;
    SUPPORT_ID?: number;
    SUPPORT_NAME?: string;
    IS_DEFAULT?: number;
    ACTIVE?: number;
    CREATED_ON?: any;
    CREATED_BY?: string;
    MODIFIED_ON?: any;
    MODIFIED_BY?: string;
}