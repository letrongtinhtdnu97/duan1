export class Supporter {
    appId?: number;
    active?: number;
    appAddress?: string;
    appCatId?: number;
    appKey?: string;
    appName?: string;
    appTypeId?: number;
    createdBy?: string;
    createdOn?: Date;
    description?: string;
    logo?: string;
    modifiedBy?: string;
    modifiedOn?: Date;
    orgCode?: string;
}

export class SSupporter {
    SUPPORT_ID?: number;
    SUPPORT_NAME?: string;
    EMAIL?: string;
    MOBILE?: string;
    EMPLOYEE_ID?: number;
    ORG_CODE?: string;
    ZALO?: string;
    SKYPE?: string;
    VIBER?: string;
    ACTIVE?: number;
    CREATED_ON?: Date;
    CREATED_BY?: string;
    MODIFIED_ON?: Date;
    MODIFIED_BY?: string;
    APPS?: Supporter[] | any;
}