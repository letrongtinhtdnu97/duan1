export interface SApp {
    APP_ID?: number;
    APP_KEY?: string;
    APP_NAME?: string;
    APP_ADDRESS?: string;
    DESCRIPTION?: string;
    LOGO?: string;
    ACTIVE?: number;
    APP_TYPE_ID?: number;
    APP_CAT_ID?: number;
}


