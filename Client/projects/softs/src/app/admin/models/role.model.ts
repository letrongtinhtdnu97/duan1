export interface Role {
    roleId?: number;
    orgCode?: string;
    roleName?: string;
    description?: string;
    active?: number;
    createdOn?: Date;
    createdBy?: string;
    modifiedOn?: Date;
    modifiedBy?: string;
    userId?: number;
    userName?: string;
}
