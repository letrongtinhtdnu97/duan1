export class User {
    id?: string;
    code?: string;
    name?: string;
    description?: string;
    price?: number;
    quantity?: number;
    inventoryStatus?: string;
    category?: string;
    image?: string;
    rating?: number;
}
export class CUser {
    USER_ID: number;
    USER_NAME: string;
    PASSWORD: string;
    PASSWORD_SALT: string;
    FULL_NAME?: string;
    EMAIL?: string;
    MOBILE?: string;
    EMPLOYEE_ID?: number;
    ORG_CODE: string;
    STATUS: number;
}

