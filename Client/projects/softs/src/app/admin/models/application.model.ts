export class Application {
    APP_ID?: number;
    ORG_CODE?: string;
    APP_NAME?: string;
    APP_ADDRESS?: string;
    DESCRIPTION?: string;
    APP_KEY?: string;
    APP_CAT_ID?: number;
    APP_TYPE_ID?: number;
    ACTIVE?: number;
    IS_PUBLIC?: number;
    CREATED_ON?: Date;
    CREATED_BY?: string;
    MODIFIED_ON?: Date;
    MODIFIED_BY?: string;
    LOGO?: SFILE[];
}
export class SApplication {
    APP_ID?: number;
    ORG_CODE?: string;
    APP_NAME?: string;
    APP_ADDRESS?: string;
    DESCRIPTION?: string;
    APP_KEY?: string;
    APP_CAT_ID?: number;
    APP_TYPE_ID?: number;
    ACTIVE?: number;
    CREATED_ON?: Date;
    CREATED_BY?: Date;
    MODIFIED_ON?: Date;
    MODIFIED_BY?: Date;
    LOGO?: string;
}

export class SFILE {
    FILE_ID?: number;
    ORG_CODE?: string;
    FILE_NAME?: string;
    FILE_TYPE?: string;
    FILE_CONTENT?: any;
    CREATED_ON?: Date;
    CREATED_BY?: string;
}

export interface APPCATEGORY {
    APP_CAT_ID?: number;
    APP_CAT_NAME?: string;
}
export interface APPTYPE {
    APP_TYPE_ID?: number;
    APP_TYPE_NAME?: string;
}

