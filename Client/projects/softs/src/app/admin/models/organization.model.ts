export class Organization {
    ORG_ID: number;
    ORG_CODE: string;
    ORG_NAME: string;
    PARENT_CODE: string;
    SHORT_NAME: string;
    MADVI_KD: string;
}
