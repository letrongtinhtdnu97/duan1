export class Menu {
    MENU_ID: number;
    MENU_NAME: string;
    ROUTE_PATH?: string;
    ICON?: string;
    PARENT_ID?: number;
    APP_ID: number;
    PRIORITY?: number;
    ACTIVE?: number;
}