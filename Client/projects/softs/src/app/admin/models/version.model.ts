export class Version {
    VERSION_ID?: number;
    VERSION_NAME?: string;
    DESCRIPTION?: string;
    RELEASE_DATE?: any;
    APP_ID?: number;
    APP_NAME?: string;
    STATUS?: number;
    CREATED_ON?: any;
    CREATED_BY?: string;
    MODIFIED_ON?: any;
    MODIFIED_BY?: string;
}
