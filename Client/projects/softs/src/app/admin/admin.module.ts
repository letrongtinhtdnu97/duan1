import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AccordionModule } from 'primeng/accordion';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ButtonModule } from 'primeng/button';
import { CalendarModule } from 'primeng/calendar';
import { CardModule } from 'primeng/card';
import { CarouselModule } from 'primeng/carousel';
import { ChartModule } from 'primeng/chart';
import { CheckboxModule } from 'primeng/checkbox';
import { ChipsModule } from 'primeng/chips';
import { CodeHighlighterModule } from 'primeng/codehighlighter';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { ColorPickerModule } from 'primeng/colorpicker';
import { ContextMenuModule } from 'primeng/contextmenu';
import { DataViewModule } from 'primeng/dataview';
import { DialogModule } from 'primeng/dialog';
import { DropdownModule } from 'primeng/dropdown';
import { FieldsetModule } from 'primeng/fieldset';
import { FileUploadModule } from 'primeng/fileupload';
import { FullCalendarModule } from 'primeng/fullcalendar';
import { GalleriaModule } from 'primeng/galleria';
import { InplaceModule } from 'primeng/inplace';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputMaskModule } from 'primeng/inputmask';
import { InputSwitchModule } from 'primeng/inputswitch';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { LightboxModule } from 'primeng/lightbox';
import { ListboxModule } from 'primeng/listbox';
import { MegaMenuModule } from 'primeng/megamenu';
import { MenuModule } from 'primeng/menu';
import { MenubarModule } from 'primeng/menubar';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { MultiSelectModule } from 'primeng/multiselect';
import { OrderListModule } from 'primeng/orderlist';
import { OrganizationChartModule } from 'primeng/organizationchart';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { PaginatorModule } from 'primeng/paginator';
import { PanelModule } from 'primeng/panel';
import { PanelMenuModule } from 'primeng/panelmenu';
import { PasswordModule } from 'primeng/password';
import { PickListModule } from 'primeng/picklist';
import { ProgressBarModule } from 'primeng/progressbar';
import { RadioButtonModule } from 'primeng/radiobutton';
import { RatingModule } from 'primeng/rating';
import { RippleModule } from 'primeng/ripple';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { SelectButtonModule } from 'primeng/selectbutton';
import { SidebarModule } from 'primeng/sidebar';
import { SlideMenuModule } from 'primeng/slidemenu';
import { SliderModule } from 'primeng/slider';
import { SplitButtonModule } from 'primeng/splitbutton';
import { StepsModule } from 'primeng/steps';
import { TabMenuModule } from 'primeng/tabmenu';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { TerminalModule } from 'primeng/terminal';
import { TieredMenuModule } from 'primeng/tieredmenu';
import { ToastModule } from 'primeng/toast';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { ToolbarModule } from 'primeng/toolbar';
import { TooltipModule } from 'primeng/tooltip';
import { TreeModule } from 'primeng/tree';
import { TreeTableModule } from 'primeng/treetable';
import { VirtualScrollerModule } from 'primeng/virtualscroller';

import { UserListComponent } from './components/user-list/user-list.component';
import { UserAddComponent } from './components/user-add/user-add.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { GroupListComponent } from './components/group-list/group-list.component';
import { GroupAddComponent } from './components/group-add/group-add.component';
import { GroupEditComponent } from './components/group-edit/group-edit.component';
import { AppEditComponent } from './components/app-edit/app-edit.component';
import { AppAddComponent } from './components/app-add/app-add.component';
import { AppListComponent } from './components/app-list/app-list.component';
import { MenuListComponent } from './components/menu-list/menu-list.component';
import { MenuAddComponent } from './components/menu-add/menu-add.component';
import { MenuEditComponent } from './components/menu-edit/menu-edit.component';
import { RoleListComponent } from './components/role-list/role-list.component';
import { RoleAddComponent } from './components/role-add/role-add.component';
import { RoleEditComponent } from './components/role-edit/role-edit.component';


import { CountryService } from '../demo/service/countryservice';
import { CustomerService } from '../demo/service/customerservice';
import { EventService } from '../demo/service/eventservice';
import { IconService } from '../demo/service/iconservice';
import { NodeService } from '../demo/service/nodeservice';
import { PhotoService } from '../demo/service/photoservice';
import { ProductService } from '../demo/service/productservice';
import { GroupService } from './services/group.service';
import { UserService } from './services/user.service';
import { BreadcrumbService } from '../core/services/breadcrumb.service';
import { MenuService } from '../app.menu.service';
import { RoleService } from './services/role.service';
import { PermissionService } from './services/permission.service';
import { OrganizationService } from './services/organization.service';
import { SupporterService } from './services/supporter.service';
import { ApplicationService } from './services/application.service';
import { MenuItemService } from './services/menu.service';
import { VersionService } from './services/version.service';
import { ComponentService } from './services/component.service';

import { AppCategoryListComponent } from './components/app-category-list/app-category-list.component';
import { AppCategoryAddComponent } from './components/app-category-add/app-category-add.component';
import { AppCategoryEditComponent } from './components/app-category-edit/app-category-edit.component';
import { SupporterEditComponent } from './components/supporter-edit/supporter-edit.component';
import { SupporterListComponent } from './components/supporter-list/supporter-list.component';
import { SupporterAddComponent } from './components/supporter-add/supporter-add.component';
import { GroupRoleAddComponent } from './components/group-role-add/group-role-add.component';
import { GroupPermissionAddComponent } from './components/group-permission-add/group-permission-add.component';
import { GroupMenuAddComponent } from './components/group-menu-add/group-menu-add.component';
import { GroupAppAddComponent } from './components/group-app-add/group-app-add.component';
import { UserAppAddComponent } from './components/user-app-add/user-app-add.component';
import { UserMenuAddComponent } from './components/user-menu-add/user-menu-add.component';
import { UserPermissionAddComponent } from './components/user-permission-add/user-permission-add.component';
import { UserRoleAddComponent } from './components/user-role-add/user-role-add.component';
import { UserGroupAddComponent } from './components/user-group-add/user-group-add.component';
import { PermissionListComponent } from './components/permission-list/permission-list.component';
import { PermissionCategoryListComponent } from './components/permission-category-list/permission-category-list.component';
import { PermissionCatAddComponent } from './components/permission-cat-add/permission-cat-add.component';
import { VersionAddComponent } from './components/version-add/version-add.component';
import { VersionListComponent } from './components/version-list/version-list.component';
import { ComponentAddComponent } from './components/component-add/component-add.component';
import { ComponentListComponent } from './components/component-list/component-list.component';

@NgModule({
  declarations: [
    UserListComponent,
    UserAddComponent,
    UserEditComponent,
    GroupListComponent,
    GroupAddComponent,
    GroupEditComponent,
    AppEditComponent,
    AppAddComponent,
    AppListComponent,
    MenuListComponent,
    MenuAddComponent,
    MenuEditComponent,
    RoleListComponent,
    RoleAddComponent,
    RoleEditComponent,
    AppCategoryListComponent,
    AppCategoryAddComponent,
    AppCategoryEditComponent,
    SupporterEditComponent,
    SupporterListComponent,
    SupporterAddComponent,
    GroupRoleAddComponent,
    GroupPermissionAddComponent,
    GroupMenuAddComponent,
    GroupAppAddComponent,
    UserAppAddComponent,
    UserMenuAddComponent,
    UserPermissionAddComponent,
    UserRoleAddComponent,
    UserGroupAddComponent,
    PermissionListComponent,
    PermissionCategoryListComponent,
    PermissionCatAddComponent,
    VersionAddComponent,
    VersionListComponent,
    ComponentAddComponent,
    ComponentListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AccordionModule,
    AutoCompleteModule,
    BreadcrumbModule,
    ButtonModule,
    CalendarModule,
    CardModule,
    CarouselModule,
    ChartModule,
    CheckboxModule,
    ChipsModule,
    CodeHighlighterModule,
    ConfirmDialogModule,
    ColorPickerModule,
    ContextMenuModule,
    DataViewModule,
    DialogModule,
    DropdownModule,
    FieldsetModule,
    FileUploadModule,
    FullCalendarModule,
    GalleriaModule,
    InplaceModule,
    InputNumberModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    LightboxModule,
    ListboxModule,
    MegaMenuModule,
    MenuModule,
    MenubarModule,
    MessageModule,
    MessagesModule,
    MultiSelectModule,
    OrderListModule,
    OrganizationChartModule,
    OverlayPanelModule,
    PaginatorModule,
    PanelModule,
    PanelMenuModule,
    PasswordModule,
    PickListModule,
    ProgressBarModule,
    RadioButtonModule,
    RatingModule,
    RippleModule,
    ScrollPanelModule,
    SelectButtonModule,
    SidebarModule,
    SlideMenuModule,
    SliderModule,
    SplitButtonModule,
    StepsModule,
    TableModule,
    TabMenuModule,
    TabViewModule,
    TerminalModule,
    TieredMenuModule,
    ToastModule,
    ToggleButtonModule,
    ToolbarModule,
    TooltipModule,
    TreeModule,
    TreeTableModule,
    VirtualScrollerModule
  ],
  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    CountryService,
    CustomerService,
    EventService,
    IconService,
    NodeService,
    PhotoService,
    ProductService,
    GroupService,
    UserService,
    MenuService,
    BreadcrumbService,
    RoleService,
    PermissionService,
    OrganizationService,
    SupporterService,
    ApplicationService,
    MenuItemService,
    VersionService,
    ComponentService
  ],
})
export class AdminModule { }
