import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { CUser } from '../../models/user.model';
import { UserService } from '../../services/user.service';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { OrganizationService } from '../../services/organization.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService, ConfirmationService]
})
export class UserListComponent implements OnInit {

  userDialog: boolean;

  lstUsers: CUser[];

  users: CUser[];

  user: CUser;

  selectedUsers: CUser[];

  submitted: boolean;

  headerName: string;

  chkActive = true;

  filterOrgs: any[];

  selectedFilterOrg: any;

  orgs: any[];

  selectedOrg: any;

  depts: any[];

  selectedDept: any;

  employs: any[];

  selectedEmploy: any;

  cols: any[];

  items: MenuItem[];

  countries: any[];

  constructor(private userService: UserService, private messageService: MessageService,
    private confirmationService: ConfirmationService, private breadcrumbService: BreadcrumbService,
    private orgService: OrganizationService) {
    this.breadcrumbService.setItems([
      { label: 'User Management' },
      { label: 'Users' }
    ]);
  }

  ngOnInit(): void {
    this.loadOrganization();
    this.loadUser();
    this.userService.getCountries().then(countries => {
      this.countries = countries;
    });

    this.cols = [
      { field: 'code', header: 'Code' },
      { field: 'name', header: 'Name' },
      { field: 'description', header: 'Description' },
      { field: 'price', header: 'Price' },
      { field: 'category', header: 'Category' },
      { field: 'rating', header: 'Reviews' },
      { field: 'inventoryStatus', header: 'Status' }
    ];

    this.items = [
      { label: 'Edit user groups', icon: 'pi pi-user-plus', command: (event: Event) => { this.openNew(); } },
      { label: 'Edit user apps', icon: 'pi pi-desktop' },
      { label: 'Edit user roles', icon: 'pi pi-globe' },
      { label: 'Edit permissions', icon: 'pi pi-id-card' },
      { label: 'Edit user menus', icon: 'pi pi-list' }
    ];
  }

  loadOrganization(): void {
    try {
      const param = {
        PARENT_CODE: 'F02'
      };

      this.orgs = [];
      this.filterOrgs = [];
      this.orgService.getOrgByParent(param).subscribe((response: any) => {
        if (response) {
          if (response.length > 0) {
            this.selectedFilterOrg = { label: 'Tất cả đơn vị', value: 'ALL' };
            this.filterOrgs.push(this.selectedFilterOrg);
            response.forEach(element => {
              this.filterOrgs.push({ label: element.ORG_CODE + ' - ' + element.ORG_NAME, value: element.ORG_CODE });
              this.orgs.push({ label: element.ORG_CODE + ' - ' + element.ORG_NAME, value: element.ORG_ID });
            });
          }
        }
      });
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Organization Load Error' });
      console.log('Org Load Error: ', error);
    }
  }

  loadUser(): void {
    try {
      this.lstUsers = [];
      this.users = [];
      this.userService.getAllUser().subscribe((response: any) => {
        if (response) {
          this.lstUsers = response;
          this.users = response;
        }
      });
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Group Load Error' });
      console.log('Group Load Error: ', error);
    }
  }

  onChangeFilterOrg(event): void {
    this.users = this.lstUsers;
    if (event.value !== 'ALL') {
      this.users = this.users.filter(val => val.ORG_CODE === event.value);
    }
  }

  openNew(): void {
    this.user = new CUser();
    this.user.USER_ID = -1;
    this.selectedOrg = null;
    this.selectedDept = null;
    this.selectedEmploy = null;
    this.submitted = false;
    this.userDialog = true;
  }

  onChangeOrg(event): void {
    try {
      const param = {
        ORG_ID: event.value
      };

      this.depts = [];
      this.orgService.getDeptByOrg(param).subscribe((response: any) => {
        if (response) {
          if (response.length > 0) {
            response.forEach(element => {
              this.depts.push({ label: element.DEPT_ID + ' - ' + element.DEPT_NAME, value: element.DEPT_ID });
            });
          }
        }
      });
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Department Load Error' });
      console.log('Department Load Error: ', error);
    }
  }

  onChangeDept(event): void {
    try {
      this.selectedEmploy = null;

      const param = {
        ORG_ID: this.selectedOrg,
        DEPT_ID: event.value
      };

      this.employs = [];
      this.orgService.getEmployeeByCondition(param).subscribe((response: any) => {
        if (response) {
          if (response.length > 0) {
            response.forEach(element => {
              this.employs.push({ label: element.EMPLOYEE_CODE + ' - ' + element.EMPLOYEE_NAME, value: element });
            });
          }
        }
      });
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Employ Load Error' });
      console.log('Employ Load Error: ', error);
    }
  }

  onChangeEmploy(): void {
    this.user.EMPLOYEE_ID = this.selectedEmploy.EMPLOYEE_ID;
    this.user.EMAIL = this.selectedEmploy.EMAIL;
    this.user.MOBILE = this.selectedEmploy.MOBILE;
    this.user.FULL_NAME = this.selectedEmploy.EMPLOYEE_NAME;
    this.user.ORG_CODE = this.selectedEmploy.ORG_CODE;
    this.user.USER_NAME = this.selectedEmploy.EMPLOYEE_CODE;
  }

  deleteSelectedUsers(): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected users?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.users = this.users.filter(val => !this.selectedUsers.includes(val));
        this.selectedUsers = null;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Products Deleted', life: 3000 });
      }
    });
  }

  editUser(user: CUser): void {
    this.user = { ...user };
    this.userDialog = true;
  }

  deleteUser(user: CUser): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + user.FULL_NAME + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.users = this.users.filter(val => val.USER_ID !== user.USER_ID);
        this.user = new CUser();
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'User Deleted', life: 3000 });
      }
    });
  }

  hideDialog(): void {
    this.userDialog = false;
    this.submitted = false;
  }

  saveUser(): void {
    this.submitted = true;
    try {
      const login: any = JSON.parse(localStorage.getItem('user'));
      const param = {
        USER_ID: this.user.USER_ID,
        USER_NAME: this.user.USER_NAME,
        PASSWORD: this.user.PASSWORD,
        FULL_NAME: this.user.FULL_NAME,
        EMAIL: this.user.EMAIL,
        MOBILE: this.user.MOBILE,
        EMPLOYEE_ID: this.user.EMPLOYEE_ID,
        CREATED_BY: login.USER_NAME,
        MODIFIED_BY: login.USER_NAME,
        ORG_CODE: this.user.ORG_CODE
      };

      if (this.user.PASSWORD === undefined || this.user.PASSWORD === '') {
        // generate random password
        const genpassword = this.userService.generatePassword(8, true, true, true);
        param.PASSWORD = genpassword;
      }

      if (this.user.USER_ID === -1) { // for add new user
        this.userService.insertUser(param).subscribe((response: any) => {
          if (response) {
            if (response.TYPE === 'SUCCESS') {
              this.userDialog = false;
              this.messageService.add({ severity: 'success', summary: 'Success Message', detail: 'User Created' });
              this.loadUser();
            } else {
              this.messageService.add({ severity: 'error', summary: 'Error Message', detail: response.MESSAGE });
            }
          }
        });
      }
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Add New User Error' });
      console.log('Add New User Error is: ', error);
    }
  }

  findIndexById(USER_ID: number): number {
    let index = -1;
    for (let i = 0; i < this.users.length; i++) {
      if (this.users[i].USER_ID === USER_ID) {
        index = i;
        break;
      }
    }

    return index;
  }
}
