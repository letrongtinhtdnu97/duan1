import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppCategoryEditComponent } from './app-category-edit.component';

describe('AppCategoryEditComponent', () => {
  let component: AppCategoryEditComponent;
  let fixture: ComponentFixture<AppCategoryEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppCategoryEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppCategoryEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
