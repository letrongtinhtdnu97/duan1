import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';

import { ApplicationService } from '../../services/application.service';
import { MenuItemService } from '../../services/menu.service';
import { SApp } from '../../models/app.model';
import { Menu } from '../../models/menu.model';

@Component({
  selector: 'app-menu-add',
  templateUrl: './menu-add.component.html',
  styleUrls: ['./menu-add.component.css']
})
export class MenuAddComponent implements OnInit {
  menuAddDialog: boolean;
  submitted: boolean;
  status: boolean;
  menu: Menu;
  currentMenu: Menu;
  icons: any[];
  filteredIcons: any [];
  selectedIcon: any;
  apps: SApp[];
  selectedApps: SApp;
  @Input() menuChildren: any;
  @Output() dathangEvent = new EventEmitter<Menu>();
  constructor(
    private appService: ApplicationService,
    private menuService: MenuItemService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService
  ) { }
  ngOnInit(): void {
    this.status = true;
    this.loadIcon();
    this.loadAppId();
  }
  loadCountPriority(parentId: any): void {
    const body = {
      PARENT_ID: parentId
    };
    this.menuService.getCountPriority(JSON.stringify(body)).subscribe(
      (data: any) => {
        console.log(data);
        if (data.TYPE === 'SUCCESS') {
          this.menu.PRIORITY = parseInt(data.MESSAGE, 10) + 1;
        }
      },
      (err: any) => {
        console.log(err);
      }
    );
  }
  openNew(): void {
    this.submitted = false;
    this.menuAddDialog = true;
    if (this.currentMenu === undefined || this.currentMenu === null) {
      const menuData = new Menu();
      this.menu = menuData;
      this.menu.PARENT_ID = 0;
      this.loadCountPriority(this.menu.PARENT_ID);
      this.selectedApps = null;
      this.selectedIcon = null;
    }else {
      const menuData = new Menu();
      this.menu = menuData;
      this.menu = {...this.currentMenu};
      console.log(this.menu);
      this.menu.PARENT_ID = this.menu.MENU_ID;
      this.menu.MENU_ID = null;
      this.menu.MENU_NAME = '';
      this.loadCountPriority(this.menu.PARENT_ID);
      this.selectedApps = null;
      this.selectedIcon = null;
    }
  }
  loadIcon(): void {
    this.menuService.getIcons().subscribe(data => {
      const icons = data;
      icons.sort((icon1, icon2) => {
          if (icon1.properties.name < icon2.properties.name) {
              return -1;
          }
          else if (icon1.properties.name < icon2.properties.name) {
              return 1;
          }
          else {
              return 0;
          }
      });

      this.icons = icons;
      this.filteredIcons = data;
    });
  }
  loadAppId(): void {
    this.menuService.getAllApp().subscribe((response: SApp[] | undefined) => {
      this.apps = response;
    });
  }
  hideDialog(): void {
    this.menuAddDialog = false;
    this.menu = null;
    this.dathangEvent.emit(this.menu);
  }
  saveMenu(): void {
    this.submitted = true;
    if (!this.selectedIcon || !this.selectedApps || !this.menu.MENU_NAME) {
      return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
    }
    const body = {
      ...this.menu,
      APP_ID: this.selectedApps.APP_ID,
      PARENT_ID: this.menu.PARENT_ID || 0,
      PRIORITY: `${this.menu.PRIORITY || 0}`,
      MODIFIED_BY: 'admin',
      ICON: this.selectedIcon.properties.name,
      CREATED_BY: 'admin'
    };
    console.log(body)
    this.menuService.insertMenuApp(JSON.stringify(body)).subscribe(
      (data: any) => {
        console.log(data);
        if (data.MESSAGE === 'OK' && data.TYPE === 'SUCCESS') {
          this.dathangEvent.emit(this.menu);
          this.hideDialog();
          this.menu = null;
          return this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Menus Add Success', life: 3000 });
        }
        return this.messageService.add({ severity: 'error', summary: 'Error2', detail: 'Error', life: 3000 });
      },
      (err: any) => {
        console.log(err);
        return this.messageService.add({ severity: 'error', summary: 'Error1', detail: 'Error', life: 3000 });
      }
    );
  }
  onFilter(event: KeyboardEvent): void {
    const searchText = (event.target as HTMLInputElement).value;

    if (!searchText) {
        this.filteredIcons = this.icons;
    }
    else {
        this.filteredIcons = this.icons.filter( it => {
            return it.icon.tags[0].includes(searchText);
        });
    }
}
}
