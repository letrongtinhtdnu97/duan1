import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { RoleService } from '../../services/role.service';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { Group } from '../../models/group.model';
import { Role } from '../../models/role.model';

@Component({
  selector: 'app-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.css'],
  providers: [MessageService, ConfirmationService]
})
export class RoleListComponent implements OnInit {

  roleDialog: boolean;
  groups: Group[];
  roles: Role[];
  role: Role;
  selectedRoles: Role[];
  status: number = 1;


  submitted: boolean;

  cols: any[];

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService, 
    private breadcrumbService: BreadcrumbService,
    private roleService: RoleService
  ) {
    this.breadcrumbService.setItems([
      { label: 'Users Management' },
      { label: 'Roles' }
    ]);
  }

  ngOnInit(): void {
    this.roleService.getAllRoles().then(data => this.roles = data).catch(e => this.roles = []);
    this.cols = [
      { field: 'roleName', header: 'Name' },
      { field: 'description', header: 'Description' },
      { field: 'userName', header: 'User' },
      { field: 'active', header: 'Status' }
    ];
  }

  openNew(): void {
    this.role = {};
    this.submitted = false;
    this.roleDialog = true;
    this.status = this.role ? 1 : 0
  }

  
  deleteSelectedRoles(): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected groups?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_ROLE: this.selectedRoles
        }
        this.roleService.deleteMulti(JSON.stringify(body))
            .then((result) => {
              if (result.MESSAGE === 'OK' && result.TYPE === 'SUCCESS'){
                this.roles = this.roles.filter(val => !this.selectedRoles.includes(val));
                this.selectedRoles = null;
                return this.showToast('success', 'Thành công', 'Xóa thành công!', 3000);
              }
              return this.showToast('error', 'Lỗi', 'Xóa không thành công!', 3000);
            })
            .catch((e) => this.showToast('error', 'Lỗi', 'Xóa không thành công!', 3000));
        // this.roles = this.groups.filter(val => !this.selectedGroups.includes(val));
        // this.selectedGroups = null;
        // this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Groups Deleted', life: 3000 });
      }
    });
  }
  editRole(role: Role): void {
    this.role = { ...role};
    this.roleDialog = true;
    this.status = this.role.active ? 1 : 0;
  }
  deleteRole(role: Role): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + role.roleName + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = { roleId: role.roleId }
        this.roleService.delete(JSON.stringify(body))
            .then((result) => {
              console.log(result)
              this.roles = this.roles.filter(val => val.roleId !== role.roleId);
              this.role = {};
              return this.showToast('success', 'Thành công', 'Xóa thành công!', 3000);
            })
            .catch((e) => this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000));
      }
    });
  }

  hideDialog(): void {
    this.roleDialog = false;
    this.submitted = false;
  }
  saveRole(): void {
    this.submitted = true;
    if(this.role) {
      if(this.role.roleId) {
        //update role
        const body = {...this.role,active: this.status ? 1: 0};
        this.roleService.update(JSON.stringify(body))
            .then((result) => {
              if(result.MESSAGE === 'OK' && result.TYPE === 'SUCCESS') {
                this.roleService.getAllRoles().then(data => this.roles = data);
                this.role = {};
                this.roleDialog = false;
                return this.showToast('success', 'Thành công', 'Tạo thành công', 3000);
              }else {
                return this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000);
              }
            })
            .catch(e => {
              console.log(e)
              return this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000);
            });
      }else {
        const body = {
          roleName: this.role.roleName,
          orgCode: 'F02',
          description: this.role.description ? this.role.description : '',
          modifiedBy: 'admin',
          createdBy: 'admin',
          active: this.status ? 1 : 0
        };
        this.roleService.insert(JSON.stringify(body))
            .then((result) => {
                if (result.MESSAGE === "OK" && result.TYPE === "SUCCESS") {
                  this.showToast('success','Thành công','Tạo thành công',3000)
                  this.roleService.getAllRoles().then(data => this.roles = data);
                  this.role = {};
                  this.roleDialog = false;
                  return;
                }else {
                  return this.showToast('error','Lỗi','Đã xảy ra lỗi',3000)
                }
            })
            .catch((e) => {
              console.log(e)
              return this.showToast('error','Lỗi','Đã xảy ra lỗi',3000)
            })
      }
    }
  }
  
  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.groups.length; i++) {
      if (this.groups[i].id === id) {
        index = i;
        break;
      }
    }

    return index;
  }

  createId(): string {
    let id = '';
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 5; i++) {
      id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
  }
  showToast(severity: string,summary: string,detail: string, life: number ): void {
    this.messageService.add({
      severity: `${severity}`,
      summary: `${summary}`,
      detail: `${detail}`,
      life: life
    });
  }
}
