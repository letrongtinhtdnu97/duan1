import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { Supporter, SSupporter } from '../../models/supporter.model';
import { SupporterService } from '../../services/supporter.service';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { OrganizationService } from '../../services/organization.service';
import { SApp } from '../../models/app.model';
@Component({
  selector: 'app-supporter-list',
  templateUrl: './supporter-list.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService, ConfirmationService]
})
export class SupporterListComponent implements OnInit {
  supporterDialog: boolean;

  supporters: SSupporter[];

  supporter: SSupporter;

  selectedSupporters: SSupporter[];

  submitted: boolean;

  cols: any[];

  items: MenuItem[];

  countries: any[];

  filteredCountries: any[];

  selectedCountryAdvanced: any[];

  filterOrgs: any[];

  selectedFilterOrg: any;

  orgs: any[] = [];

  selectedOrg: any;
  selectedApps: SApp[];
  apps: SApp[];
  depts: any[] = [];
  employs: any[] = [];
  value: any;
  selectedEmploy: any;
  myName: string;
  constructor(private supporterService: SupporterService,
              private messageService: MessageService,
              private orgService: OrganizationService,
              private confirmationService: ConfirmationService,
              private breadcrumbService: BreadcrumbService) {
      this.breadcrumbService.setItems([
      { label: 'App Management' },
      { label: 'Supporters' }
    ]);
  }
  ngOnInit(): void {
    this.loadOrganization();
    this.loadData();
    this.loadApp();
    this.cols = [
      { field: 'SUPPORT_NAME', header: 'SUPPORT_NAME' },
      { field: 'EMAIL', header: 'EMAIL' },
      { field: 'SKYPE', header: 'SKYPE' },
      { field: 'APP_NAME', header: 'APP_NAME' },
    ];

    this.items = [
      { label: 'Edit support apps', icon: 'pi pi-microsoft', command: (event: Event) => { this.openNew(); } }
    ];
  }
  loadData(): void {
    try {
      this.supporters = [];
      this.supporterService.getAllSupporter().subscribe((response: any) => {
        if (response) {
          this.supporters = response;
          console.log(response);
        }
      });
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Group Load Error' });
      console.log('Supporter Load Error: ', error);
    }
  }
  loadApp(): void {
    try {
      this.supporterService.getAllApp().subscribe((response: SApp[] | undefined) => {
        this.apps = response;
      });
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'App Name Error' });
      console.log('App Name Error: ', error);
    }
  }
  loadOrganization(): void {
    try {
      const param = {
        PARENT_CODE: 'F02'
      };

      this.orgs = [];
      this.filterOrgs = [];
      this.orgService.getOrgByParent(param).subscribe((response: any) => {
        if (response) {
          if (response.length > 0) {
            this.selectedFilterOrg = { label: 'Tất cả đơn vị', value: 'ALL' };
            this.filterOrgs.push(this.selectedFilterOrg);
            response.forEach(element => {
              this.filterOrgs.push({ label: element.ORG_CODE + ' - ' + element.ORG_NAME, value: element.ORG_CODE });
              this.orgs.push({ label: element.ORG_CODE + ' - ' + element.ORG_NAME, value: element.ORG_ID });
            });
          }
        }
      });
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Organization Load Error' });
      console.log('Org Load Error: ', error);
    }
  }
  openNew(): void {
    this.supporter = {};
    this.submitted = false;
    this.supporterDialog = true;
    this.selectedApps = [];
    this.selectedEmploy = null;
    this.selectedFilterOrg = null;
    this.selectedOrg = null;
    this.depts = [];

  }

  deleteSelectedSupporters(): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        try {
          const arrId = this.selectedSupporters.map((item: SSupporter) => {
            return {
              SUPPORT_ID: item.SUPPORT_ID
            };
          });
          const body = {
            LIST_SUP: arrId
          };
          this.supporterService
            .deleteSupporter(JSON.stringify(body))
            .subscribe((arg) => {
              if (arg.MESSAGE === 'OK' && arg.TYPE === 'SUCCESS') {
                this.loadData();
                this.supporter = {};
                this.selectedSupporters = [];
                this.showToast('success', 'Thành công', 'Cập nhật thành công!', 3000);
              } else {
                this.showToast('error', 'Thất bại', 'Vui lòng thử lại sau!', 3000);
              }
            });
        } catch (error) {
          this.showToast('error', 'Thất bại', 'Vui lòng thử lại sau!', 3000);
          console.log(error);
        }
      }
    });
  }
  editSupporter(supporter: SSupporter): void {
    this.supporter = { ...supporter };
    this.supporterDialog = true;
    this.selectedEmploy = null;
    this.selectedFilterOrg = null;
    this.selectedOrg = null;
    this.depts = [];
    let data = [];
    let results = [];
    if (this.supporter.APPS) {
      data = this.supporter.APPS.map((i: Supporter) => i);
      results =  this.apps.filter(({ APP_ID: id1 }) => data.some(({ appId: id2 }) => id2 === id1));
    }
    this.selectedApps = results;
  }
  onChangeOrg(event): void {
    try {
      const param = {
        ORG_ID: event.value
      };

      this.depts = [];
      this.orgService.getDeptByOrg(param).subscribe((response: any) => {
        if (response) {
          if (response.length > 0) {
            response.forEach(element => {
              this.depts.push({ label: element.DEPT_ID + ' - ' + element.DEPT_NAME, value: element.DEPT_ID });
            });
          }
        }
      });
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Department Load Error' });
      console.log('Department Load Error: ', error);
    }
  }
  onChangeDept(event): void {
    try {
      this.selectedEmploy = null;

      const param = {
        ORG_ID: this.selectedOrg,
        DEPT_ID: event.value
      };

      this.employs = [];
      this.orgService.getEmployeeByCondition(param).subscribe((response: any) => {
        if (response) {
          if (response.length > 0) {
            response.forEach(element => {
              this.employs.push({ label: element.EMPLOYEE_CODE + ' - ' + element.EMPLOYEE_NAME, value: element });
            });
          }
        }
      });
    } catch (error) {
      this.messageService.add({ severity: 'error', summary: 'Error Message', detail: 'Employ Load Error' });
      console.log('Employ Load Error: ', error);
    }
  }
  onChangeEmploy(): void {
    this.supporter.EMAIL = this.selectedEmploy.EMAIL;
    this.supporter.MOBILE = this.selectedEmploy.MOBILE;
  }
  deleteSupporter(supporter: SSupporter): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + supporter.SUPPORT_NAME + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_SUP: [
            {
              SUPPORT_ID: supporter.SUPPORT_ID
            }
        ]};
        this.supporterService
          .deleteSupporter(JSON.stringify(body))
          .subscribe(arg => {
            if (arg.MESSAGE === 'OK' && arg.TYPE === 'SUCCESS') {
              this.loadData();
              this.supporter = {};
              this.messageService.add({ severity: 'success', summary: 'Thành công', detail: 'Cập nhật thành công!' });
            } else {
              this.messageService.add({ severity: 'error', summary: 'Thất bại', detail: 'Vui lòng thử lại sau!' });
            }
          });
      }
    });
  }

  hideDialog(): void {
    this.supporterDialog = false;
    this.submitted = false;
  }

  saveSupporter(): void {

    this.submitted = true;
    if (!this.supporter) {
      this.supporterDialog = false;
      this.supporter = {};
      return;

    }
    console.log(this.supporter);
    if (this.supporter.EMAIL) {
      if (this.supporter.SUPPORT_ID) {
        try {
          console.log(this.selectedApps);
          const body = {
            ...this.supporter,
            LIST_APP: this.selectedApps
          };
          this.supporterService
            .updateSupporter(JSON.stringify(body))
            .subscribe(arg => {
              if (arg.MESSAGE === 'OK' && arg.TYPE === 'SUCCESS') {
                this.showToast('success', 'Thành công', 'Cập nhật thành công!', 3000);
                this.loadData();
                this.supporter = {};
                this.submitted = false;
                this.supporterDialog = false;
              }else {
                this.showToast('error', 'Thất bại', 'Vui lòng thử lại sau!', 3000);
                console.log(arg);
              }
            });
        } catch (error) {
         this.showToast('error', 'Thất bại', 'Vui lòng thử lại sau!', 3000);
        }
         }
         else {
           try {
             let bodyInsert = {};
             if (this.selectedApps === undefined || this.selectedApps === [] || this.selectedApps === null) {
                bodyInsert = {
                  ...this.supporter,
                  SUPPORT_NAME: this.selectedEmploy.EMPLOYEE_NAME,
                  CREATED_BY: 'admin',
                  MODIFIED_BY: 'admin',
                  ORG_CODE: `${this.selectedOrg}`,
                  EMPLOYEE_ID: this.selectedEmploy.EMPLOYEE_ID
                };
             }else {
              const listApp = this.selectedApps.map(item => {
                return {
                  APP_ID: item.APP_ID
                };
              });
              bodyInsert = {
                ...this.supporter,
                SUPPORT_NAME: this.selectedEmploy.EMPLOYEE_NAME,
                CREATED_BY: 'admin',
                MODIFIED_BY: 'admin',
                ORG_CODE: `${this.selectedOrg}`,
                EMPLOYEE_ID: this.selectedEmploy.EMPLOYEE_ID,
                LIST_APP: listApp
              };
             }
             this.supporterService
               .insertSupporter(JSON.stringify(bodyInsert))
               .subscribe(arg => {
                 if (arg.MESSAGE === 'OK' && arg.TYPE === 'SUCCESS') {
                   this.showToast('success', 'Thành công', 'Cập nhật thành công!', 3000);
                   this.loadData();
                   this.supporter = {};
                   this.submitted = false;
                   this.supporterDialog = false;
                 }else {
                   this.showToast('error', 'Thất bại', 'Vui lòng thử lại sau!', 3000);
                   console.log(arg);
                 }
               });
           } catch (error) {
             this.showToast('error', 'Thất bại', 'Vui lòng thử lại sau!', 3000);
             console.log(error);
           }
       }
    }
  }
  showToast(severity: string, summary: string, detail: string, life: number ): void {
    this.messageService.add({
      severity: `${severity}`,
      summary: `${summary}`,
      detail: `${detail}`,
      life
    });
  }

}
