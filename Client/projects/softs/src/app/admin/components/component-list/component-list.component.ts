import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { Product } from '../../../demo/domain/product';
import { ProductService } from '../../../demo/service/productservice';
import { Permission } from '../../models/permission.model';
import { ComponentAddComponent } from '../component-add/component-add.component';
import { ComponentService } from '../../services/component.service';
import { SComponent } from '../../models/component.model';
@Component({
  selector: 'app-component-list',
  templateUrl: './component-list.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: ['./component-list.component.css'],
  providers: [MessageService, ConfirmationService]
})
export class ComponentListComponent implements OnInit {

  components: SComponent[];

  component: SComponent;

  selectedComponents: SComponent[];

  cols: any[];

  items: MenuItem[];

  @ViewChild('componentAdd') componentAdd: ComponentAddComponent;

  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private componentService: ComponentService
    ) {
    this.breadcrumbService.setItems([
      { label: 'App Management' },
      { label: 'Components' }
    ]);
  }

  ngOnInit(): void {
    this.loadData();
    this.cols = [
      { field: 'COMPONENT_NAME', header: 'COMPONENT_NAME' },
      { field: 'DESCRIPTION', header: 'DESCRIPTION' },
      { field: 'APP_NAME', header: 'APP_NAME' },
      { field: 'SUPPORT_NAME', header: 'SUPPORT_NAME' },
      { field: 'ACTIVE', header: 'ACTIVE' }
    ];
  }
  loadData(): void {
    this.componentService.getAllComponent()
      .subscribe(
        (data: SComponent[]) => {
          console.log(data);
          this.components = data;
        },
        (err: any) => {
          console.log(err);
        }
      );
  }
  openNew(): void {
    this.componentAdd.openNew();
  }
  editComponent(component: SComponent): void {
    this.componentAdd.component = component;
    this.componentAdd.editComponent();
  }
  delSelectedComponent(): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected products?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        try {
          const arrId = this.selectedComponents.map((item: SComponent) => {
            return {
              COMPONENT_ID: item.COMPONENT_ID
            };
          });
          const body = {
            LIST_COMPONENT: arrId
          };
          this.componentService
            .delete(JSON.stringify(body))
            .subscribe((arg) => {
              if (arg.MESSAGE === 'OK' && arg.TYPE === 'SUCCESS') {
                this.loadData();
                this.component = {};
                this.selectedComponents = [];
                return this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Successful', life: 3000 });
              } else {
                return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
              }
            });
        } catch (error) {
          console.log(error);
          return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
        }
      }
    });
  }
  deleteComponent(component: SComponent): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + component.COMPONENT_NAME + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_COMPONENT: [component]
        };
        this.componentService.delete(JSON.stringify(body)).subscribe(
          (data: any) => {
            if (data.TYPE === 'SUCCESS' && data.MESSAGE === 'OK') {
              this.loadData();
              return this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Successful', life: 3000 });
            }
            return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          },
          (err: any) => {
            return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          }
        );
      }
    });
  }
}
