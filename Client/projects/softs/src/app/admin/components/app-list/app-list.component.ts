import { Component, OnInit, ViewChild } from '@angular/core';
import { FileUpload } from 'primeng/fileupload';

import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { MenuItem, SelectItem } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { Application, SFILE, APPTYPE, APPCATEGORY } from '../../models/application.model';
import { ApplicationService } from '../../services/application.service';

import { GroupService } from '../../services/group.service';

import { Group } from '../../models/group.model';
@Component({
  selector: 'app-app-list',
  templateUrl: './app-list.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService, ConfirmationService]
})
export class AppListComponent implements OnInit {
  @ViewChild('fileInput') fileInput: FileUpload;
  appDialog: boolean;
  appCategories: APPCATEGORY[];
  appCategory: APPCATEGORY;
  selectAppCategory: APPCATEGORY;
  appTypes: APPTYPE[];
  appType: APPTYPE;
  selectAppType: APPTYPE;
  apps: Application[];
  appDefault: any[];
  app: Application;
  status: number = 1;
  selectedApps: Application[];

  submitted: boolean;

  cols: any[];

  items: MenuItem[];

  categories: SelectItem[];

  types: SelectItem[];

  selectedCategory: SelectItem;

  selectedType: SelectItem;

  languages: any[];

  selectedlanguages: string[] = [];

  fileLoad: SFILE[];
  selectedFile: SFILE;

  srcIMG: any;
  groupDialog: boolean;

  groups: Group[];

  group: Group;

  selectedGroups: Group[];
  imageBlobUrl: string | ArrayBuffer;

  uploadedFiles: any[] = [];
  fileImage: any = null;
  appCatId: any;
  constructor(private messageService: MessageService, private applicationService: ApplicationService,private groupService: GroupService,
    private confirmationService: ConfirmationService, private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'Application Management' },
      { label: 'Applications' }
    ]);
  }

  ngOnInit(): void {
    this.loadData();
    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'description', header: 'Description' },
      { field: 'quantity', header: 'Users' },
      { field: 'inventoryStatus', header: 'Status' }
    ];
  }
  openNew(): void {
    this.app = {};
    this.submitted = false;
    this.appDialog = true;
    this.status = this.app ? 1 : 0;
  }
  onUpload(event): void {
    for (const file of event.files) {
        this.fileImage = file;
    }
    console.log('this.fileImage', this.fileImage);
  }
  deleteSelectedApp(): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected apps?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_APP: this.selectedApps,
          MODIFIED_BY: 'admin'
        };
        console.log(body);
        this.applicationService.deleteAppList(JSON.stringify(body)).subscribe(
          (data) => {
            console.log(data);
            this.loadData();
            this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Group Deleted', life: 3000 });
          },
          (err) => {
            this.loadData();
            console.log(err);
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error Deleted', life: 3000 });
          }
        );
      }
    });
  }

  editApp(app: Application): void {
    this.app = { ...app };
    console.log(this.apps);
    console.log(app);
    this.appDialog = true;
    this.status = this.app.ACTIVE ? 1 : 0;
    console.log('this.appCategories', this.appCategories);
    this.selectAppCategory = this.appCategories.filter(val => val.APP_CAT_ID === app.APP_CAT_ID)[0];
    this.selectAppType = this.appTypes.filter(val => val.APP_TYPE_ID === app.APP_TYPE_ID)[0];
  }

  deleteApp(app: Application): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete app',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_APP : [
            {
              APP_ID: app.APP_ID
            }
          ],
          MODIFIED_BY: 'admin'
        };
        this.applicationService.deleteAppList(JSON.stringify(body)).subscribe(
          (data) => {
            console.log(data);
            this.loadData();
            this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Group Deleted', life: 3000 });
          },
          (err) => {
            this.loadData();
            console.log(err);
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error Deleted', life: 3000 });
          }
        );
      }
    });
  }
  hideDialog(): void {
    this.appDialog = false;
    this.submitted = false;
    this.selectAppCategory = {};
  }

  saveApp(): void {
    this.submitted = true;
    if (!this.selectAppType || !this.selectAppCategory || !this.app.APP_NAME || !this.app.APP_KEY || !this.app.DESCRIPTION) {
      return;
    }
    if (this.app.APP_ID) {
      console.log('this.app.APP_ID', this.app.APP_ID);
      let formData: FormData = new FormData();
      if (this.fileInput._files.length > 0) {
        formData.append('FILE_CONTENT', this.fileInput._files[0], this.fileInput._files[0].name);
        formData.append('MODIFIED_BY', 'admin');
        formData.append('ACTIVE', this.status ? `1` : `0`);
        formData.append('APP_ID', `${this.app.APP_ID}`);
        formData.append('ORG_CODE', 'F02');
        formData.append('APP_KEY', this.app.APP_KEY);
        formData.append('CREATED_BY', this.app.CREATED_BY);
        formData.append('APP_NAME', this.app.APP_NAME);
        formData.append('APP_ADDRESS', this.app.APP_ADDRESS);
        formData.append('DESCRIPTION', this.app.DESCRIPTION);
        formData.append('APP_TYPE_ID', `${this.selectAppType.APP_TYPE_ID}`);
        formData.append('APP_CAT_ID', `${this.selectAppCategory.APP_CAT_ID}`);
        this.applicationService.updateApplist(formData).subscribe(
          (data) => {
            if (data.MESSAGE === 'OK' && data.TYPE === 'SUCCESS') {
              this.loadData();
              this.selectAppCategory = {};
              this.selectAppType = {};
              this.selectedApps = null;
              this.hideDialog();
              this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Thanh cong', life: 3000 });
            } else {
              this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
            }
          },
          (err) => {
            console.log(err);
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          }
        );
      }else {
        
        formData.append('FILE_CONTENT', null);
        formData.append('MODIFIED_BY', 'admin');
        formData.append('ACTIVE', this.status ? `1` : `0`);
        formData.append('APP_ID', `${this.app.APP_ID}`);
        formData.append('ORG_CODE', 'F02');
        formData.append('APP_KEY', this.app.APP_KEY);
        formData.append('CREATED_BY', this.app.CREATED_BY);
        formData.append('APP_NAME', this.app.APP_NAME);
        formData.append('APP_ADDRESS', this.app.APP_ADDRESS);
        formData.append('DESCRIPTION', this.app.DESCRIPTION);
        formData.append('APP_TYPE_ID', `${this.selectAppType.APP_TYPE_ID}`);
        formData.append('APP_CAT_ID', `${this.selectAppCategory.APP_CAT_ID}`);
        this.applicationService.updateApplist(formData).subscribe(
          (data) => {
            if (data.MESSAGE === 'OK' && data.TYPE === 'SUCCESS') {
              this.loadData();
              this.selectAppCategory = {};
              this.selectAppType = {};
              this.selectedApps = null;
              this.hideDialog();
              this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Thanh cong', life: 3000 });
            } else {
              this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
            }
          },
          (err) => {
            console.log(err);
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          }
        );
      }
      //update
    }else {
      if (this.fileInput._files.length > 0) {
        const formData: FormData = new FormData();
        formData.append('FILE_CONTENT', this.fileInput._files[0], this.fileInput._files[0].name);
        formData.append('CREATED_BY', 'admin');
        formData.append('ORG_CODE', 'F02');
        formData.append('APP_KEY', this.app.APP_KEY);
        formData.append('CREATED_BY', this.app.CREATED_BY);
        formData.append('APP_NAME', this.app.APP_NAME);
        formData.append('APP_ADDRESS', this.app.APP_ADDRESS);
        formData.append('DESCRIPTION', this.app.DESCRIPTION);
        formData.append('APP_TYPE_ID', `${this.selectAppType.APP_TYPE_ID}`);
        formData.append('APP_CAT_ID', `${this.selectAppCategory.APP_CAT_ID}`);
        this.applicationService.insertApplist(formData).subscribe(
          (data) => {
            console.log(data);
            if (data.MESSAGE === 'OK' && data.TYPE === 'SUCCESS') {
              this.loadData();
              this.selectAppCategory = {};
              this.selectAppType = {};
              this.hideDialog();
              this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Thanh cong', life: 3000 });
            } else {
              this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
            }
          },
          (err) => {
            console.log(err);
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          }
        );
      } else {
        const body = {
          ...this.app,
          APP_TYPE_ID: this.selectAppType.APP_TYPE_ID,
          APP_CAT_ID: this.selectAppCategory.APP_CAT_ID,
          MODIFIED_BY: 'admin',
          ORG_CODE: 'F02',
          CREATED_BY: 'admin'
        };
        this.applicationService.insertAppListNoImage(body).subscribe(
          (data) => {
            if (data.MESSAGE === 'OK' && data.TYPE === 'SUCCESS') {
              this.loadData();
              this.selectAppCategory = {};
              this.selectAppType = {};
              this.hideDialog();
              this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Thanh cong', life: 3000 });
            } else {
              this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error ', life: 3000 });
            }
          },
          (err) => {
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error ', life: 3000 });
            console.log(err);
          }
        );
      }
      // if (this.fileImage) {
      //   console.log('co imgae')
      // }else {
      //   console.log('no image')
      // }
    }
  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.groups.length; i++) {
      if (this.groups[i].id === id) {
        index = i;
        break;
      }
    }

    return index;
  }
  convertByteToB64code(data1: any, name: string): any {
    const ascii = new Uint8Array(data1);
    const base64 = btoa(
      new Uint8Array(ascii)
        .reduce((data, byte) => data + String.fromCharCode(byte), '')
    );
    let image = '';
    image = `data:image/${name};base64,${base64}`;
    return image;
  }
  createImageFromBlob(image: Blob): void {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.imageBlobUrl = reader.result;
    }, false);
    if (image) {
      console.log(image);
      reader.readAsDataURL(image);
    }
  }
  loadData(): any {
    this.applicationService.getAllAppName()
        .subscribe(
          (data: Application[]) => {
            this.apps = data;
          },
          (err: any) => {
            console.log(err);
            this.apps = [];
          }
        );
    this.applicationService.getAllAppCategory()
        .subscribe(
          (data: APPCATEGORY[]) => {
            this.appCategories = data;
          },
          (err: any) => {
            console.log(err);
            this.appCategories = [];
          }
        );
    this.applicationService.getAllAppType()
        .subscribe(
          (data: APPTYPE[]) => {
            console.log(data);
            this.appTypes = data;
          },
          (err: any) => {
            console.log(err);
            this.appTypes = [];
          }
        );
  }
 
  onBasicUpload(event): void {
    this.messageService.add({ severity: 'info', summary: 'Success', detail: 'File Uploaded with Basic Mode' });
  }
}
