import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Permission } from '../../models/permission.model';
import { PermissionService } from '../../services/permission.service';
import { VersionService } from '../../services/version.service';
import { Version } from '../../models/version.model';
import { MenuItemService } from '../../services/menu.service';
import { SApp } from '../../models/app.model';
@Component({
  selector: 'app-version-add',
  templateUrl: './version-add.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService]
})
export class VersionAddComponent implements OnInit {

  versionDialog: boolean;
  apps: SApp[];
  app: SApp;
  selectApp: SApp;
  submitted: boolean;
  dateValue: Date;
  per: Permission;
  versions: Version[];
  version: Version;
  categories: any[];

  selectCategory: any;

  // tslint:disable-next-line:no-output-rename
  // @Output('loadPermissionByApp') loadPermissionByApp: EventEmitter<any> = new EventEmitter();
  @Output() callEvent = new EventEmitter<any>();
  constructor(
    private perService: PermissionService,
    private msgService: MessageService,
    private menuService: MenuItemService,
    private versionService: VersionService
  ) { }

  ngOnInit(): void {
    this.loadAppId();
  }
//   ngAfterViewInit(): void {
// if (this.version) {
//     this.selectApp = this.apps.filter(val => this.version.APP_ID = val.APP_ID)[0];
//     this.dateValue = new Date(this.version.RELEASE_DATE);
//   } else {
//     this.version = new Version();
//     this.selectApp = null;
//   }
//   }
  hideDialog(): void {
    this.submitted = false;
    this.version = null;
    this.selectApp = null;
    this.versionDialog = false;
  }
  convertDate(): string {
    let twoDigitMonth = this.dateValue.getMonth() + 1 + '';
    if (twoDigitMonth.length === 1){
        twoDigitMonth = '0' + twoDigitMonth;
    }
    let twoDigitDate = this.dateValue.getDate() + '';
    if (twoDigitDate.length === 1){
      twoDigitDate = '0' + twoDigitDate;
    }
    const currentDate = twoDigitDate + '/' + twoDigitMonth + '/' + this.dateValue.getFullYear();
    return currentDate;
  }
  loadAppId(): void {
    this.menuService.getAllApp().subscribe((response: SApp[] | undefined) => {
      this.apps = response;
      if (this.version && this.version.VERSION_ID) {
        this.selectApp = this.apps.filter(val => this.version.APP_ID === val.APP_ID)[0];
        this.dateValue = new Date(this.version.RELEASE_DATE);
      } else {
        this.version = new Version();
        this.selectApp = null;
        this.dateValue = null;
      }
    });
  }
  saveVersion(): void {
    if (!this.version.VERSION_NAME ) {
      return this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
    }
    if (this.version.VERSION_ID) {
      const body = {
        ...this.version,
        APP_ID: this.selectApp.APP_ID,
        RELEASE_DATE: this.convertDate(),
        MODIFIED_BY: 'admin'
      };
      console.log(body);
      this.versionService.updateVersion(JSON.stringify(body))
      .subscribe(
        (data: any) => {
          console.log(data);
          if (data.TYPE === 'SUCCESS' && data.MESSAGE === 'OK') {
            this.dateValue = null;
            this.version = {};
            this.selectApp = null;
            this.hideDialog();
            this.callEvent.emit(1);
            return this.msgService.add({ severity: 'success', summary: 'Successful', detail: 'Version Insert Successs', life: 3000 });
          }
          return this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
        },
        (err: any) => {
          console.log(err);
          return this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
        }
      );
    }else {
      const body = {
        ...this.version,
        RELEASE_DATE: this.convertDate(),
        APP_ID: this.selectApp.APP_ID,
        CREATED_BY: 'admin',
        MODIFIED_BY: 'admin',
      };
      console.log(body);
      this.versionService.insertVersion(JSON.stringify(body))
        .subscribe(
          (data: any) => {
            if (data.TYPE === 'SUCCESS' && data.MESSAGE === 'OK') {
              //this.loadData();
              this.dateValue = null;
              this.version = {};
              this.selectApp = null;
              this.hideDialog();
              this.callEvent.emit(1);
              return this.msgService.add({ severity: 'success', summary: 'Successful', detail: 'Version Insert Successs', life: 3000 });
            }
            return this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          },
          (err: any) => {
            return this.msgService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          }
        );
    }
  }
}
