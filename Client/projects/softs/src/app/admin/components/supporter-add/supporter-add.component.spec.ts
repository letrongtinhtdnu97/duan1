import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupporterAddComponent } from './supporter-add.component';

describe('SupporterAddComponent', () => {
  let component: SupporterAddComponent;
  let fixture: ComponentFixture<SupporterAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupporterAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupporterAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
