import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserMenuAddComponent } from './user-menu-add.component';

describe('UserMenuAddComponent', () => {
  let component: UserMenuAddComponent;
  let fixture: ComponentFixture<UserMenuAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserMenuAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserMenuAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
