import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Permission } from '../../models/permission.model';
import { PermissionService } from '../../services/permission.service';

@Component({
  selector: 'app-permission-cat-add',
  templateUrl: './permission-cat-add.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: ['./permission-list.component.css'],
  providers: [MessageService]
})
export class PermissionCatAddComponent implements OnInit {

  perCategoryDialog: boolean;

  submitted: boolean;

  per: Permission;

  categories: any[];

  selectCategory: any;

  // tslint:disable-next-line:no-output-rename
  // @Output('loadPermissionByApp') loadPermissionByApp: EventEmitter<any> = new EventEmitter();

  constructor(private perService: PermissionService, private msgService: MessageService) { }

  ngOnInit(): void {
  }

}
