import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionCatAddComponent } from './permission-cat-add.component';

describe('PermissionCatAddComponent', () => {
  let component: PermissionCatAddComponent;
  let fixture: ComponentFixture<PermissionCatAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PermissionCatAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionCatAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
