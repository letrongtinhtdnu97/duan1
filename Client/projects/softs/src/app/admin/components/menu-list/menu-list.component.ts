import { Component, OnInit, ViewChild } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { MenuItem } from 'primeng/api';
import { Menu } from '../../models/menu.model';
import { MenuItemService } from '../../services/menu.service';
import { Application } from '../../models/application.model';


import { ApplicationService } from '../../services/application.service';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { MenuAddComponent } from '../menu-add/menu-add.component';
import { MenuEditComponent} from '../menu-edit/menu-edit.component';
import { Permission } from '../../models/permission.model';
export interface TreeNode {
  data?: any;
  children?: TreeNode[];
  leaf?: boolean;
  expanded?: boolean;
}
@Component({
  selector: 'app-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService, ConfirmationService, MenuItemService]
})
export class MenuListComponent implements OnInit {

  menuDialog: boolean;

  apps: Application[];

  app: Application;

  menus: Menu[];

  menu: Menu;

  selectedMenus: Menu[];

  submitted: boolean;

  cols: any[];

  items: MenuItem[];

  countries: any[];

  statuses: any[];

  filteredCountries: any[];

  selectedCountryAdvanced: any[];

  files2: TreeNode[];

  selectedFiles2: TreeNode[];

  @ViewChild('menuAdd') menuAdd: MenuAddComponent;
  @ViewChild('menuEdit') menuEdit: MenuEditComponent;

  constructor(private appService: ApplicationService, private menuService: MenuItemService,
              private messageService: MessageService,
              private confirmationService: ConfirmationService, private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'App Management' },
      { label: 'Menus' }
    ]);
  }

  ngOnInit(): void {
    this.loadData();
    // this.menuService.getCountries().then(countries => {
    //   this.countries = countries;
    // });

    // this.menuService.getFilesystem().then(files => {
    //   console.log(files);
    //   this.files2 = files;
    // });

    this.cols = [
      { field: 'MENU_ID', header: 'Menu ID' },
      { field: 'MENU_NAME', header: 'Name' },
      { field: 'ICON', header: 'Icon' },
      { field: 'ROUTE_PATH', header: 'Path' },
      { field: 'PRIORITY', header: 'Priority' }
    ];

    this.items = [
      { label: 'Edit user groups', icon: 'pi pi-user-plus', command: (event: Event) => { this.openNew(); } },
      { label: 'Edit user apps', icon: 'pi pi-desktop' },
      { label: 'Edit user roles', icon: 'pi pi-globe' },
      { label: 'Edit permissions', icon: 'pi pi-id-card' },
      { label: 'Edit user menus', icon: 'pi pi-list' }
    ];

    this.statuses = [
      { label: 'Unqualified', value: 'unqualified' },
      { label: 'Qualified', value: 'qualified' },
      { label: 'New', value: 'new' },
      { label: 'Negotiation', value: 'negotiation' },
      { label: 'Renewal', value: 'renewal' },
      { label: 'Proposal', value: 'proposal' }
    ];
  }
  loadData(): void {
    this.menuService.getAllMenu().subscribe(
      (data: any) => {
        console.log(data);
        this.files2 = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }
  openNew(): void {
    this.menuAdd.openNew();
    // this.menuAdd.per = new Permission();
    // this.menuAdd.per.PER_ID = -1;
    // this.menuAdd.menuDialog = true;
  }

  deleteSelectedMenus(): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected menus?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const arr = this.selectedFiles2.map((item: TreeNode) => item.data);
        const body = {
          LIST_MENU: arr,
          MODIFIED_BY: 'admin'
        };
        console.log(body);
        this.menuService.deleteMenuObj(JSON.stringify(body)).subscribe(
          (data: any) => {
            if (data.MESSAGE === 'OK' && data.TYPE === 'SUCCESS') {
              this.loadData();
              this.selectedFiles2 = null;
              this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Menus Deleted', life: 3000 });
            }else {
              this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Menus Deleted Error', life: 3000 });
            }
          },
          (err: any) => {
            console.log(err);
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Menus Deleted Error', life: 3000 });
          }
        );
      }
    });
  }

  editmenu(menu: Menu): void {
    this.menuDialog = true;
    this.menuEdit.menu = {...menu};
    this.menuEdit.openNew();
  }
  notifyMessage($event): void {
    console.log($event);
  }
  addMenuChildren(menu: any): void {
    this.menuAdd.currentMenu = menu;
    this.menuAdd.openNew();
  }
  deletemenu(menu: Menu): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + menu.MENU_NAME + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_MENU: [menu],
          MODIFIED_BY: 'admin'
        };
        console.log(body)
        this.menuService.deleteMenuObj(JSON.stringify(body)).subscribe(
          (data: any) => {
            console.log(data);
            if (data.MESSAGE === 'OK' && data.TYPE === 'SUCCESS') {
              this.loadData();
              this.selectedFiles2 = null;
              this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Menus Deleted', life: 3000 });
            }else {
              this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Menus Deleted Error', life: 3000 });
            }
          },
          (err: any) => {
            console.log(err);
            this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Menus Deleted Error', life: 3000 });
          }
        );
      }
    });
  }

  hideDialog(): void {
    this.menuDialog = false;
    this.submitted = false;
  }

  saveMenu(): void {
  }
  createId(): string {
    let id = '';
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 5; i++) {
      id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
  }

  filterCountry(event): void {
    const filtered: any[] = [];
    const query = event.query;
    // for (let i = 0; i < this.countries.length; i++) {
    //   const country = this.countries[i];
    //   if (country.name.toLowerCase().indexOf(query.toLowerCase()) == 0) {
    //     filtered.push(country);
    //   }
    // }

    for (const country of this.countries) {
      if (country.name.toLowerCase().indexOf(query.toLowerCase()) === 0) {
        filtered.push(country);
      }
    }
    this.filteredCountries = filtered;
  }
}
