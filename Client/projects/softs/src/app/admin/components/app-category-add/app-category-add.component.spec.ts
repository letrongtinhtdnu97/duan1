import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppCategoryAddComponent } from './app-category-add.component';

describe('AppCategoryAddComponent', () => {
  let component: AppCategoryAddComponent;
  let fixture: ComponentFixture<AppCategoryAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppCategoryAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppCategoryAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
