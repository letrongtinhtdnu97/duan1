import { Component, OnInit,Output,EventEmitter  } from '@angular/core';
import { Menu } from '../../models/menu.model';
import { MenuItemService } from '../../services/menu.service';
import { SApp } from '../../models/app.model';
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-menu-edit',
  templateUrl: './menu-edit.component.html',
  styleUrls: ['./menu-edit.component.css']
})
export class MenuEditComponent implements OnInit {

  menuAddDialog: boolean;
  submitted: boolean;
  status: boolean;
  menu: Menu;
  apps: SApp[];
  icons: any[];
  filteredIcons: any [];
  selectedIcon: any;
  selectedApps: SApp;
  @Output() editEvent = new EventEmitter<Menu>();
  constructor(
    private menuService: MenuItemService,
    private messageService: MessageService,
  ) { }
  ngOnInit(): void {
    this.status = true;
    this.loadIcon();
    this.loadAppId();
  }
  openNew(): void {
    this.submitted = false;
    this.menuAddDialog = true;
    this.selectedIcon = this.filteredIcons.filter(val => val.properties.name === this.menu.ICON)[0];
    this.selectedApps = this.apps.filter(val => val.APP_ID === this.menu.APP_ID)[0]
  }
  hideDialog(): void {
    this.menuAddDialog = false;
  }
  loadIcon(): void {
    this.menuService.getIcons().subscribe(data => {
      const icons = data;
      icons.sort((icon1, icon2) => {
          if (icon1.properties.name < icon2.properties.name) {
              return -1;
          }
          else if (icon1.properties.name < icon2.properties.name) {
              return 1;
          }
          else {
              return 0;
          }
      });

      this.icons = icons;
      this.filteredIcons = data;
    });
  }
  saveMenu(): void {
    console.log('edit');
    console.log(this.filteredIcons);
    const body = {
      MODIFIED_BY: 'admin',
      ROUTE_PATH: this.menu.ROUTE_PATH,
      PRIORITY: `${this.menu.PRIORITY || 1}`,
      PARENT_ID: this.menu.PARENT_ID,
      APP_ID: this.selectedApps.APP_ID,
      MENU_NAME: this.menu.MENU_NAME,
      MENU_ID: this.menu.MENU_ID,
      ICON: this.selectedIcon.properties.name
    };
    this.menuService.updateMenu(body)
      .subscribe(
        (data: any) => {
          console.log(data);
          if (data.MESSAGE === 'OK' && data.TYPE === 'SUCCESS') {
            this.editEvent.emit(this.menu);
            this.hideDialog();
            this.menu = null;
            return this.messageService.add({ severity: 'success', summary: 'Success', detail: 'Menus edit Success', life: 3000 });
           }
          return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Menus edit error', life: 3000 });
        },
        (err: any) => {
          console.log(err);
          return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Menus edit error', life: 3000 });
        }
      );
  }
  loadAppId(): void {
    this.menuService.getAllApp().subscribe((response: SApp[] | undefined) => {
      this.apps = response;
    });
  }

}
