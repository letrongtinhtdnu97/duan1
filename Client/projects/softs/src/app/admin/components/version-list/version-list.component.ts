import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { Product } from '../../../demo/domain/product';
import { ProductService } from '../../../demo/service/productservice';
import { Permission } from '../../models/permission.model';
import { VersionAddComponent } from '../version-add/version-add.component';
import { VersionService } from '../../services/version.service';
import { Version } from '../../models/version.model';
@Component({
  selector: 'app-version-list',
  templateUrl: './version-list.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: ['./version-list.component.css'],
  providers: [MessageService, ConfirmationService]
})
export class VersionListComponent implements OnInit {

  versions: Version[];

  version: Version;

  selectedVersions: Version[];

  cols: any[];
  colsPending: any [];
  @ViewChild('versionAdd') versionAdd: VersionAddComponent;

  constructor(
    private perService: ProductService,
    private messageService: MessageService,
    private versionService: VersionService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'App Management' },
      { label: 'Versions' }
    ]);
  }

  ngOnInit(): void {
    this.loadData();
    this.colsPending = [
      {id: 1, field: 'instock'},
      {id: 0, field: 'outofstock'}
    ];
  }

  openNew(): void {
    this.versionAdd.version = new Version();
    this.versionAdd.selectApp = {};
    this.versionAdd.loadAppId();
    this.versionAdd.versionDialog = true;
  }
  editVersion(version: Version): void {
    this.versionAdd.version = version;
    console.log(this.versionAdd.version.VERSION_ID);
    this.versionAdd.loadAppId();
    this.versionAdd.versionDialog = true;
  }
  convertDate(dataTime: Date): string {
    const dateTimezone = new Date(dataTime);
    let twoDigitMonth = dateTimezone.getMonth() + 1 + '';
    if (twoDigitMonth.length === 1){
        twoDigitMonth = '0' + twoDigitMonth;
    }
    let twoDigitDate = dateTimezone.getDate() + '';
    if (twoDigitDate.length === 1){
      twoDigitDate = '0' + twoDigitDate;
    }
    const currentDate = twoDigitDate + '/' + twoDigitMonth + '/' + dateTimezone.getFullYear();
    return currentDate;
  }
  loadData(): any {
    this.versionService.getAllVersion().subscribe(
      (data: Version[]) => {
        console.log(data);
        this.versions = data;
      },
      (err: any) => {
        console.log(err);
        this.versions = [];
      }
    );
  }
  deleteSelectVersion(): any {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected versions?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_VERSION: this.selectedVersions
        };
        this.versionService.deleteVersion(JSON.stringify(body)).subscribe(
          (data: any) => {
            if (data.TYPE === 'SUCCESS' && data.MESSAGE === 'OK') {
              this.loadData();
              return this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Successs', life: 3000 });
            }
            return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          },
          (err: any) => {
            return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          }
        );
      }
    });
  }
  deleteVersion(version: Version): any {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + version.VERSION_NAME + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_VERSION: [version]
        };
        this.versionService.deleteVersion(JSON.stringify(body)).subscribe(
          (data: any) => {
            if (data.TYPE === 'SUCCESS' && data.MESSAGE === 'OK') {
              this.loadData();
              return this.messageService.add(
                { severity: 'success', summary: 'Successful', detail: 'Version Deleted Successs', life: 3000 });
            }
            return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          },
          (err: any) => {
            return this.messageService.add({ severity: 'error', summary: 'Error', detail: 'Error', life: 3000 });
          }
        );
      }
    });
  }
}
