import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupporterEditComponent } from './supporter-edit.component';

describe('SupporterEditComponent', () => {
  let component: SupporterEditComponent;
  let fixture: ComponentFixture<SupporterEditComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupporterEditComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupporterEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
