import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupAppAddComponent } from './group-app-add.component';

describe('GroupAppAddComponent', () => {
  let component: GroupAppAddComponent;
  let fixture: ComponentFixture<GroupAppAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupAppAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupAppAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
