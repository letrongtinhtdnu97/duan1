import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { Group } from '../../models/group.model';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: ['./group-list.component.css'],
  providers: [MessageService, ConfirmationService]
})
export class GroupListComponent implements OnInit {

  groupDialog: boolean;

  groups: Group[];

  group: Group;

  selectedGroups: Group[];

  submitted: boolean;

  cols: any[];

  constructor(private groupService: GroupService, private messageService: MessageService,
              private confirmationService: ConfirmationService, private breadcrumbService: BreadcrumbService) {
    this.breadcrumbService.setItems([
      { label: 'User Management' },
      { label: 'Groups' }
    ]);
  }

  ngOnInit(): void {
    this.groupService.getGroups().then(data => this.groups = data);

    this.cols = [
      { field: 'name', header: 'Name' },
      { field: 'description', header: 'Description' },
      { field: 'quantity', header: 'Users' },
      { field: 'inventoryStatus', header: 'Status' }
    ];
  }

  openNew(): void {
    this.group = {};
    this.submitted = false;
    this.groupDialog = true;
  }

  deleteSelectedGroup(): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected groups?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.groups = this.groups.filter(val => !this.selectedGroups.includes(val));
        this.selectedGroups = null;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Groups Deleted', life: 3000 });
      }
    });
  }

  editGroup(group: Group): void {
    this.group = { ...group };
    this.groupDialog = true;
  }

  deleteGroup(group: Group): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + group.name + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.groups = this.groups.filter(val => val.id !== group.id);
        this.group = {};
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Group Deleted', life: 3000 });
      }
    });
  }

  hideDialog(): void {
    this.groupDialog = false;
    this.submitted = false;
  }

  saveGroup(): void {
    this.submitted = true;

    if (this.group.name.trim()) {
      if (this.group.id) {
        this.groups[this.findIndexById(this.group.id)] = this.group;
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Group Updated', life: 3000 });
      }
      else {
        this.group.id = this.createId();
        this.groups.push(this.group);
        this.messageService.add({ severity: 'success', summary: 'Successful', detail: 'Group Created', life: 3000 });
      }

      this.groups = [...this.groups];
      this.groupDialog = false;
      this.group = {};
    }
  }

  findIndexById(id: string): number {
    let index = -1;
    for (let i = 0; i < this.groups.length; i++) {
      if (this.groups[i].id === id) {
        index = i;
        break;
      }
    }

    return index;
  }

  createId(): string {
    let id = '';
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < 5; i++) {
      id += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return id;
  }
}
