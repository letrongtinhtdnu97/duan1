import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupRoleAddComponent } from './group-role-add.component';

describe('GroupRoleAddComponent', () => {
  let component: GroupRoleAddComponent;
  let fixture: ComponentFixture<GroupRoleAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupRoleAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupRoleAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
