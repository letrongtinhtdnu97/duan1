import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';

import { PermissionCategory, Permission } from '../../models/permission.model';
import { PermissionService } from '../../services/permission.service';
@Component({
  selector: 'app-permission-list',
  templateUrl: './permission-list.component.html',
  styleUrls: ['./permission-list.component.css'],
  providers: [MessageService, ConfirmationService]
})
export class PermissionListComponent implements OnInit {
  permissions: Permission[] = [];
  permission: Permission;
  selectPermissions: Permission[];
  submitted: boolean;
  selectCategory: PermissionCategory = {};
  permissionDialog: boolean;
  categories: PermissionCategory[];
  cols: any[];
  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private perService: PermissionService
    ) {
    this.breadcrumbService.setItems([
      { label: 'Permission User' },
      { label: 'Permission' }
    ]);
    this.cols = [
        { field: 'perCode', header: 'perCode' },
        { field: 'perName', header: 'perName' },
        { field: 'perCatName', header: 'perCatName' },
    ];
  }

  ngOnInit(): void {
    this.showData();
  }
  showData(): void {
    this.perService
      .getAllPermission().subscribe(
        (data: Permission[] ) => {
          console.log(data);
          this.permissions = data;
        },
        (err: any) => {
          console.log(err);
          this.permissions = [];
        }
      );
    this.perService
      .getAllPermissionCategories().subscribe(
        (data: PermissionCategory[]) => {
          console.log(data);
          this.categories = data;
        },
        (err: any) => {
          console.log(err);
          this.categories = [];
        }
      );
  }
  hideDialog(): void {
    this.permissionDialog = false,
    this.permission = {};
    this.selectCategory = {};
    this.submitted = false;
  }
  editPermission(permission: Permission): void {
    this.permission = { ...permission};
    console.log(this.permission);
    this.selectCategory = this.categories.filter(val => val.perCatId === permission.PER_CAT_ID)[0];
    console.log('this.selectCategory', this.selectCategory);
    this.permissionDialog = true;
  }
  deletePermission(permission: Permission): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + permission.PER_NAME + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = permission;
        this.perService.deletePermission(body)
            .then((result) => {
              console.log(result);
              this.permissions = this.permissions.filter(val => val.perId !== permission.perId);
              this.permission = {};
              return this.showToast('success', 'Thành công', 'Xóa thành công!', 3000);
            })
            .catch((e) => this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000));
      }
    });
  }
  openNew(): void {
    this.permission = {};
    this.selectCategory = {};
    this.permissionDialog = true;
    this.submitted = false;
  }
  savePermission(): void {
    this.submitted = true;
    if (this.permission.PER_CODE && this.permission.PER_NAME) {
      if (this.permission.perId) {
        let bodyUpdate = {};
        if (this.selectCategory?.perCatId) {
          bodyUpdate = {
            ...this.permission,
            modifiedBy: 'admin',
            perCatId: this.selectCategory.perCatId
          };
        }else {
          delete this.permission.PER_CAT_ID;
          bodyUpdate = {
            ...this.permission,
            modifiedBy: 'admin',
          };
        }
        this.perService
          .updatePermission(bodyUpdate)
          .then((result) => {
            if (result.MESSAGE === 'OK' && result.TYPE === 'SUCCESS') {
              this.perService
              .getAllPermission()
              .then(data => {
                this.permissions = data;
                this.permission = {};
                this.selectCategory = {};
                this.permissionDialog = false;
                return this.showToast('success', 'Thành công', 'Cập nhật thành công', 3000);
              })
              .catch(e => this.permissions = []);
            }else {
              return this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000);
            }
          })
          .catch(e  => {
            console.log(e);
            this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000);
          });
      }else {
        let bodyParam = {};
        if (this.selectCategory) {
          bodyParam = {
            ...this.permission,
            createdBy: 'admin',
            modifiedBy: 'admin',
            orgCode: this.permission.PER_CODE,
            perCatId: this.selectCategory.perCatId
          };
        }else {
          bodyParam = {
            ...this.permission,
            createdBy: 'admin',
            orgCode: this.permission.PER_CODE,
            modifiedBy: 'admin',
          };
        }
        this.perService.insertPermission(bodyParam)
          .then((result) => {
            if (result.MESSAGE === 'OK' && result.TYPE === 'SUCCESS') {
              this.perService
              .getAllPermission()
              .then(data => {
                this.permissions = data;
                this.permission = {};
                this.permissionDialog = false;
                return this.showToast('success', 'Thành công', 'Tạo thành công', 3000);
              })
              .catch(e => this.permissions = []);
            }else {
              return this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000);
            }
          });
      }
    }
  }
  deleteSelectedPermissions(): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected groups?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_PER: this.selectPermissions
        };
        this.perService.deleteMultiplePermission(JSON.stringify(body))
            .then((result) => {
              if (result.MESSAGE === 'OK' && result.TYPE === 'SUCCESS'){
                this.permissions = this.permissions.filter(val => !this.selectPermissions.includes(val));
                this.selectPermissions = null;
                return this.showToast('success', 'Thành công', 'Xóa thành công!', 3000);
              }
              return this.showToast('error', 'Lỗi', 'Xóa không thành công!', 3000);
            })
            .catch((e) => this.showToast('error', 'Lỗi', 'Xóa không thành công!', 3000));
      }
    });
  }
  showToast(severity: string, summary: string, detail: string, life: number ): void {
    this.messageService.add({
      severity: `${severity}`,
      summary: `${summary}`,
      detail: `${detail}`,
      life
    });
  }

}
