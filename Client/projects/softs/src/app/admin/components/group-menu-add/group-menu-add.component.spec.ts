import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupMenuAddComponent } from './group-menu-add.component';

describe('GroupMenuAddComponent', () => {
  let component: GroupMenuAddComponent;
  let fixture: ComponentFixture<GroupMenuAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroupMenuAddComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupMenuAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
