import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PermissionCategoryListComponent } from './permission-category-list.component';

describe('PermissionCategoryListComponent', () => {
  let component: PermissionCategoryListComponent;
  let fixture: ComponentFixture<PermissionCategoryListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PermissionCategoryListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PermissionCategoryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
