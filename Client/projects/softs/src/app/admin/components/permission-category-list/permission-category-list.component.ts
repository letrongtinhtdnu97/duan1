import { Component, OnInit } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';

import { PermissionCategory } from '../../models/permission.model';
import { PermissionService } from '../../services/permission.service';
@Component({
  selector: 'app-permission-category-list',
  templateUrl: './permission-category-list.component.html',
  styleUrls: ['./permission-category-list.component.css'],
  providers: [MessageService, ConfirmationService]
})
export class PermissionCategoryListComponent implements OnInit {
  permissions: PermissionCategory[] = []
  permission: PermissionCategory
  selectPermissions: PermissionCategory[]
  submitted: boolean;
  permissionDialog: boolean;
  cols: any[];
  constructor(
    private messageService: MessageService,
    private confirmationService: ConfirmationService,
    private breadcrumbService: BreadcrumbService,
    private perService: PermissionService
    ) {
    this.breadcrumbService.setItems([
      { label: 'Permission User' },
      { label: 'Permission' }
    ]);
  this.cols = [
    { field: 'perCatCode', header: 'perCatCode' },
    { field: 'perCatName', header: 'perCatName' }
  ];
  }

  ngOnInit(): void {
    this.loadData();
  }
  hideDialog(): void {
    this.permissionDialog = false,
    this.permission = {};
    this.submitted = false;
  }
  editPermission(permission: PermissionCategory): void {
    this.permission = { ...permission};
    this.permissionDialog = true;
  }
  loadData(): void {
    this.perService
      .getAllPermissionCategories().subscribe((res: PermissionCategory[]) => {
        this.permissions = res;
      });
  }
  deletePermission(permission: PermissionCategory): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete ' + permission.perCatName + '?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = { perId: permission.perCatId }
        this.perService.deletePermissionCategory(JSON.stringify(body))
            .then((result) => {
              if (result.MESSAGE === 'OK' && result.TYPE === 'SUCCESS'){
                this.permissions = this.permissions.filter(val => val.perCatId !== permission.perCatId);
                this.permission = {};
                return this.showToast('success', 'Thành công', 'Xóa thành công!', 3000);
              }else {
                return this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000);
              }
            })
            .catch((e) => this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000));
      }
    });
  }
  openNew(): void {
    this.permission = {};
    this.permissionDialog = true;
    this.submitted = false;
  }
  savePermission(): void {
    if (this.permission) {
      this.submitted = true;
      if (this.permission.perCatId) {
        const body = { ...this.permission };
        this.perService.updatePermissionCategogy(JSON.stringify(body))
          .then((result) => {
            if (result.MESSAGE === 'OK' && result.TYPE === 'SUCCESS') {
              this.loadData();
              this.permission = {};
              this.permissionDialog = false;
              return this.showToast('success', 'Thành công', 'Cập nhật thành công!', 3000);
            }else {
              return this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000);
            }
          })
          .catch(e => {
            return this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000);
          });
      }else {
        const body = {
          ...this.permission,
          orgCode: this.permission.perCatCode
        };
        this.perService
          .insertPermissionCategory(body)
          .then(result => {
            if (result.MESSAGE === 'OK' && result.TYPE === 'SUCCESS') {
              this.loadData();
              this.permission = {};
              this.permissionDialog = false;
              return this.showToast('success', 'Thành công', 'Cập nhật thành công!', 3000);
            }else {
              return this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000);
            }
          })
          .catch(e =>  {
            console.log(e)
            this.showToast('error', 'Lỗi', 'Đã xảy ra lỗi', 3000)
          });
      }
    }
  }
  // saveRole
  deleteSelectedPermissions(): void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected groups?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        const body = {
          LIST_PER: this.selectPermissions
        };
        this.perService.deleteMultiPermission(JSON.stringify(body))
            .then((result) => {
              if (result.MESSAGE === 'OK' && result.TYPE === 'SUCCESS'){
                this.permissions = this.permissions.filter(val => !this.selectPermissions.includes(val));
                this.selectPermissions = null;
                return this.showToast('success', 'Thành công', 'Xóa thành công!', 3000);
              }
              return this.showToast('error', 'Lỗi', 'Xóa không thành công!', 3000);
            })
            .catch((e) => this.showToast('error', 'Lỗi', 'Xóa không thành công!', 3000));
      }
    });
  }
  showToast(severity: string, summary: string, detail: string, life: number ): void {
    this.messageService.add({
      severity: `${severity}`,
      summary: `${summary}`,
      detail: `${detail}`,
      life
    });
  }
}
