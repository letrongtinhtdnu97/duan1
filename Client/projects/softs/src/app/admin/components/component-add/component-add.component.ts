import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { MessageService } from 'primeng/api';
import { Permission } from '../../models/permission.model';
import { MenuItemService } from '../../services/menu.service';
import { SApp } from '../../models/app.model';
import { SSupporter } from '../../models/supporter.model';
import { SupporterService } from '../../services/supporter.service';
import { SComponent } from '../../models/component.model';
import { ComponentService } from '../../services/component.service';
@Component({
  selector: 'app-component-add',
  templateUrl: './component-add.component.html',
  styleUrls: ['../../../../assets/scss/tabledemo.scss'],
  styles: [`
        :host ::ng-deep .p-dialog .product-image {
            width: 150px;
            margin: 0 auto 2rem auto;
            display: block;
        }
    `],
  providers: [MessageService]
})
export class ComponentAddComponent implements OnInit {
  apps: SApp[];
  app: SApp;
  selectApp: SApp;
  componentDialog: boolean;
  supporters: SSupporter[];

  supporter: SSupporter;

  selectedSupporters: SSupporter;
  submitted: boolean;

  per: Permission;

  categories: any[];
  components: SComponent[];

  component: SComponent;

  selectedComponents: SComponent[];
  selectCategory: any;
  status: number;
  // tslint:disable-next-line:no-output-rename
  // @Output('loadPermissionByApp') loadPermissionByApp: EventEmitter<any> = new EventEmitter();
  @Output() addEvent = new EventEmitter<any>();
  constructor(
    private msgService: MessageService,
    private menuService: MenuItemService,
    private supporterService: SupporterService,
    private componentService: ComponentService
  ) { }

  ngOnInit(): void {
    this.status = 1;
    this.loadAppId();
    this.loadSupporter();
  }

  loadSupporter(): void {
    try {
      this.supporters = [];
      this.componentService.getAllSupporterToComponent().subscribe((response: any) => {
        if (response) {
          this.supporters = response;
        }
      });
    } catch (error) {
      this.msgService.add({ severity: 'error', summary: 'Error Message', detail: 'Error' });
      console.log('Supporter Load Error: ', error);
    }
  }
  loadAppId(): void {
    this.menuService.getAllApp()
      .subscribe(
        (data: SApp[]) => {
          console.log(data);
          this.apps = data;
        }
      );
  }
  openNew(): void {
    this.component = new SComponent();
    this.componentDialog = true;
  }
  editComponent(): void {
    this.selectApp = this.apps.filter((val: SApp) =>  val.APP_ID === this.component.APP_ID)[0];
    this.selectedSupporters = this.supporters.filter((val: SSupporter) => val.SUPPORT_ID === this.component.SUPPORT_ID)[0];
    this.status = this.component.ACTIVE;
    this.componentDialog = true;
  }
  hideDialog(): void {
    this.submitted = false;
    this.selectApp = null;
    this.selectedSupporters = null;
    this.componentDialog = false;
  }

  saveComponent(component: SComponent): void {
    this.submitted = true;
    if (!component.COMPONENT_NAME || !this.selectApp || !this.selectedSupporters) {
      return this.msgService.add({ severity: 'error', summary: 'Error Message', detail: 'Error' });
    }
    if (component.COMPONENT_ID) {
      const body = {
        ...component,
        IS_DEFAULT: 1,
        ACTIVE: this.status ? 1 : 0,
        SUPPORT_ID: this.selectedSupporters.SUPPORT_ID,
        MODIFIED_BY: 'admin',
        APP_ID: this.selectApp.APP_ID
      };
      this.componentService.update(JSON.stringify(body))
        .subscribe(
          (data: any) => {
            console.log(data);
            if (data.TYPE === 'SUCCESS' && data.MESSAGE === 'OK') {
              this.addEvent.emit(1);
              this.component = null;
              this.selectApp = null;
              this.selectedSupporters = null;
              this.hideDialog();
              return this.msgService.add({ severity: 'success', summary: 'Success Message', detail: 'Success' });
            } else {
              return this.msgService.add({ severity: 'error', summary: 'Error Message', detail: 'Error' });
            }
          },
          (err: any) => {
            return this.msgService.add({ severity: 'error', summary: 'Error Message', detail: 'Error' });
          }
        );
    } else {
      const body = {
        ...this.component,
        IS_DEFAULT: 1,
        ACTIVE: this.status ? 1 : 0,
        SUPPORT_ID: this.selectedSupporters.SUPPORT_ID,
        MODIFIED_BY: 'admin',
        CREATED_BY: 'admin',
        APP_ID: this.selectApp.APP_ID
      };
      this.componentService.insert(JSON.stringify(body))
        .subscribe(
          (data: any) => {
            console.log(data)
            if (data.TYPE === 'SUCCESS' && data.MESSAGE === 'OK') {
              this.addEvent.emit(1);
              this.component = null;
              this.selectApp = null;
              this.selectedSupporters = null;
              this.hideDialog();
              return this.msgService.add({ severity: 'success', summary: 'Success Message', detail: 'Success' });
            } else {
              return this.msgService.add({ severity: 'error', summary: 'Error Message', detail: 'Error' });
            }
          }
        );
    }
  }

}
