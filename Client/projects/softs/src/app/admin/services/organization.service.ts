import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { Organization } from '../models/organization.model';

@Injectable()
export class OrganizationService {
  protected url = 'http://127.0.0.1:7101/ServiceHR-HR-context-root/resources/ServiceHR/';

  constructor(private http: HttpClient) { }

  getOrgByParent(param: any): any {
    return this.http.post(this.url + 'getOrgByParent', param);
  }

  getDeptByOrg(param: any): any {
    return this.http.post(this.url + 'getDeptByOrg', param);
  }

  getEmployeeByOrg(param: any): any {
    return this.http.post(this.url + 'getEmployeeByOrg', param);
  }

  getEmployeeByCondition(param: any): any {
    return this.http.post(this.url + 'getEmployeeByCondition', param);
  }
}
