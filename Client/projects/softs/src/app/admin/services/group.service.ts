import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Group } from '../models/group.model';

@Injectable()
export class GroupService {

  constructor(private http: HttpClient) { }

  getGroupsSmall(): any {
    return this.http.get<any>('assets/demo/data/products-small.json')
      .toPromise()
      .then(res => res.data as Group[])
      .then(data => data);
  }

  getGroups(): any {
    return this.http.get<any>('assets/demo/data/products.json')
      .toPromise()
      .then(res => res.data as Group[])
      .then(data => data);
  }

  getGroupsMixed(): any {
    return this.http.get<any>('assets/demo/data/products-mixed.json')
      .toPromise()
      .then(res => res.data as Group[])
      .then(data => data);
  }

  getGroupsWithOrdersSmall(): any {
    return this.http.get<any>('assets/demo/data/products-orders-small.json')
      .toPromise()
      .then(res => res.data as Group[])
      .then(data => data);
  }
}
