import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Supporter } from '../models/supporter.model';
const baseUrl = `http://127.0.0.1:7101/ServiceQTHT-QTHT-context-root/resources/ServiceQTHT`;
const option = {
    'Content-Type': 'application/json'
};
@Injectable()
export class SupporterService {

  constructor(private http: HttpClient) { }

  getAllSupporter(): any {
    return this.http.get<any>(`${baseUrl}/getAllSupporter`);
  }

  insertSupporter(data: any): any {
    return this.http.post<any>(`${baseUrl}/insertSupporter`, data, {headers : option});
  }
  updateSupporter(data: any): any {
    return this.http.post<any>(`${baseUrl}/updateSupporter`, data, {headers : option});
  }
  deleteSupporter(data: any): any {
    return this.http.post<any>(`${baseUrl}/deleteSuppoter`, data, {headers : option});
  }
  getAllApp(): any {
    return this.http.get<any>(`${baseUrl}/getAllApp`);
  }
  // example
  getSupportersSmall(): any {
    return this.http.get<any>('assets/demo/data/products-small.json')
      .toPromise()
      .then(res => res.data as Supporter[])
      .then(data => data);
  }

  getSupporters(): any {
    return this.http.get<any>('assets/demo/data/products.json')
      .toPromise()
      .then(res => res.data as Supporter[])
      .then(data => data);
  }

  getSupportersMixed(): any {
    return this.http.get<any>('assets/demo/data/products-mixed.json')
      .toPromise()
      .then(res => res.data as Supporter[])
      .then(data => data);
  }

  getSupportersWithOrdersSmall(): any {
    return this.http.get<any>('assets/demo/data/products-orders-small.json')
      .toPromise()
      .then(res => res.data as Supporter[])
      .then(data => data);
  }

  getCountries(): any {
    return this.http.get<any>('assets/demo/data/countries.json')
      .toPromise()
      .then(res => res.data as any[])
      .then(data => data);
  }
}
