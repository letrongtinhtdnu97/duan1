import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
const option = {
  'Content-Type': 'application/json'
};
@Injectable({
  providedIn: 'root'
})

export class VersionService {
  protected url = `http://127.0.0.1:7101/ServiceQTHT-QTHT-context-root/resources/ServiceQTHT/`;
  constructor(private http: HttpClient) { }

  getAllVersion(): any {
    return this.http.get(this.url + 'getAllVersion');
  }
  insertVersion(data: string): any {
    return this.http.post(this.url + 'insertVersion', data, {headers: option});
  }
  updateVersion(data: string): any {
    return this.http.post(this.url + 'updateVersion', data, {headers: option});
  }
  deleteVersion(data: string): any {
    return this.http.post(this.url + 'deleteVersion', data, {headers: option});
  }
}
