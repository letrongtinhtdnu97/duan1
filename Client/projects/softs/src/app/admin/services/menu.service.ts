import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {TreeNode} from 'primeng/api';
import {map} from 'rxjs/operators';
import { Menu } from '../models/menu.model';
const baseUrl = `http://127.0.0.1:7101/ServiceQTHT-QTHT-context-root/resources/ServiceQTHT`;
const option = {
    'Content-Type': 'application/json'
};
@Injectable()
export class MenuItemService {
  constructor(private http: HttpClient) { }

  apiUrl = 'assets/demo/data/icons.json';
  icons: any[];
  getIcons(): any {
      return this.http.get(this.apiUrl).pipe(map((response: any) => {
          this.icons = response.icons;
          return this.icons;
      }));
  }

  getAllMenu(): any {
    return this.http.get<any>(`${baseUrl}/getAllMenu`);
  }
  getAllMenuByAppId(body: string): any {
    return this.http.post<any>(`${baseUrl}/getAllMenuByAppId`, body, {headers: option});
  }
  deleteMenuObj(data: any): any {
    return this.http.post<any>(`${baseUrl}/deleteMenuObj`, data, {
      headers: option
    });
  }
  insertMenuApp(data: any): any {
    return this.http.post<any>(`${baseUrl}/insertMenuApp`, data, {
      headers: option
    });
  }

  updateMenu(data: any): any {
    return this.http.post<any>(`${baseUrl}/updateMenu`, data, {
      headers: option
    });
  }
  getAllApp(): any {
    return this.http.get<any>(`${baseUrl}/getAllApp`);
  }
  getCountPriority(data: any): any {
    return this.http.post<any>(`${baseUrl}/getCountPriority`, data , {headers: option});
  }
  getMenusSmall(): any {
    return this.http.get<any>('assets/demo/data/products-small.json')
      .toPromise()
      .then(res => res.data as Menu[])
      .then(data => data);
  }

  getMenus(): any {
    return this.http.get<any>('assets/demo/data/products.json')
      .toPromise()
      .then(res => res.data as Menu[])
      .then(data => data);
  }

  getMenusMixed(): any {
    return this.http.get<any>('assets/demo/data/products-mixed.json')
      .toPromise()
      .then(res => res.data as Menu[])
      .then(data => data);
  }

  getMenusWithOrdersSmall(): any {
    return this.http.get<any>('assets/demo/data/products-orders-small.json')
      .toPromise()
      .then(res => res.data as Menu[])
      .then(data => data);
  }

  getCountries(): any {
    return this.http.get<any>('assets/demo/data/countries.json')
      .toPromise()
      .then(res => res.data as any[])
      .then(data => data);
  }

  getFiles(): any {
    return this.http.get<any>('assets/demo/data/files.json')
      .toPromise()
      .then(res => res.data as TreeNode[]);
  }

  getLazyFiles(): any {
    return this.http.get<any>('assets/demo/data/files-lazy.json')
      .toPromise()
      .then(res => res.data as TreeNode[]);
  }

  getFilesystem(): any {
    return this.http.get<any>('assets/demo/data/filesystem.json')
      .toPromise()
      .then(res => res.data as TreeNode[]);
  }

  getLazyFilesystem(): any {
    return this.http.get<any>('assets/demo/data/filesystem-lazy.json')
      .toPromise()
      .then(res => res.data as TreeNode[]);
  }
}