import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { PermissionCategory, Permission } from '../models/permission.model'
const baseUrl = `http://127.0.0.1:7101/ServiceQTHT-QTHT-context-root/resources/ServiceQTHT`;
const option = {
    'Content-Type': 'application/json'
};
@Injectable()
export class  PermissionService {

  constructor(private http: HttpClient) { }

  getAllPermissionCategories(): any {
    return this.http.get<any>(`${baseUrl}/getAllPermissionCategories`);
  }
  getAllPermission(): any {
    return this.http.get<any>(`${baseUrl}/getAllPermission`);
  }

  insertPermissionCategory(body: PermissionCategory): any {
    return this.http.post<any>(`${baseUrl}/insertPermissionCategories`, body, {headers : option})
      .toPromise()
      .then(res => res)
      .then(data => data);
  }
  insertPermission(body: Permission): any {
    return this.http.post<any>(`${baseUrl}/insertPermission`, body, {headers : option})
      .toPromise()
      .then(res => res)
      .then(data => data);
  }
  deletePermission(body: Permission): any {
    return this.http.post<any>(`${baseUrl}/deletePermission`, body, {headers : option})
      .toPromise()
      .then(res => res)
      .then(data => data);
  }
  deleteMultiplePermission(body: string): any {
    return this.http.post<any>(`${baseUrl}/deleteMultiplePermission`, body, {headers : option})
      .toPromise()
      .then(res => res)
      .then(data => data);
  }
  updatePermission(body: Permission): any {
    return this.http.post<any>(`${baseUrl}/updatePermission`, body, {headers : option})
      .toPromise()
      .then(res => res)
      .then(data => data);
  }
  updatePermissionCategogy(body: string): any {
    return this.http.post<any>(`${baseUrl}/updatePermissionCategory`,body,{headers : option})
      .toPromise()
      .then(res => res)
      .then(data => data);
  }
  deletePermissionCategory(body: string):any {
    return this.http.post<any>(`${baseUrl}/deletePermissionCategory`,body,{headers : option})
      .toPromise()
      .then(res => res)
      .then(data => data);
  }
  deleteMultiPermission(body: string):any {
    return this.http.post<any>(`${baseUrl}/deleteMultiPermission`,body,{headers : option})
    .toPromise()
    .then(res => res)
    .then(data => data);
  }
  
}
