import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CUser } from '../models/user.model';

@Injectable()
export class UserService {
  protected url = `http://127.0.0.1:7101/ServiceQTHT-QTHT-context-root/resources/ServiceQTHT/`;

  constructor(private http: HttpClient) { }

  getAllUser(): any {
    return this.http.get(this.url + 'getAllUser');
  }

  getAttUser(param: any): any {
    return this.http.post(this.url + 'getAttByUser', param);
  }

  public getUserByCondition(param: any): any {
    return this.http.post(this.url + 'getUserByCondition', param);
  }

  public getUserDetail(param: any): any {
    return this.http.post(this.url + 'getUserDetail', param);
  }

  public insertUser(param: any): any {
    return this.http.post(this.url + 'insertUser', param);
  }

  public updateUser(param: any): any {
    return this.http.post(this.url + 'updateUser', param);
  }

  generatePassword(length: number, hasUpper: boolean, hasNumbers: boolean, hasSymbols: boolean): string {
    const passwordLength = length || 12;
    const addUpper = hasUpper;
    const addNumbers = hasNumbers;
    const addSymbols = hasSymbols;

    // tslint:disable-next-line:max-line-length
    const lowerCharacters = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
    // tslint:disable-next-line:max-line-length
    const upperCharacters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    const numbers = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
    const symbols = ['!', '?', '@'];

    const getRandom = array => array[Math.floor(Math.random() * array.length)];

    let finalCharacters = '';

    if (addUpper) {
      finalCharacters = finalCharacters.concat(getRandom(upperCharacters));
    }

    if (addNumbers) {
      finalCharacters = finalCharacters.concat(getRandom(numbers));
    }

    if (addSymbols) {
      finalCharacters = finalCharacters.concat(getRandom(symbols));
    }

    for (let i = 1; i < passwordLength - 3; i++) {
      finalCharacters = finalCharacters.concat(getRandom(lowerCharacters));
    }

    return finalCharacters.split('').sort(() => 0.5 - Math.random()).join('');
  }

  getUsersSmall(): any {
    return this.http.get<any>('assets/demo/data/products-small.json')
      .toPromise()
      .then(res => res.data as CUser[])
      .then(data => data);
  }

  getUsers(): any {
    return this.http.get<any>('assets/demo/data/products.json')
      .toPromise()
      .then(res => res.data as CUser[])
      .then(data => data);
  }

  getUsersMixed(): any {
    return this.http.get<any>('assets/demo/data/products-mixed.json')
      .toPromise()
      .then(res => res.data as CUser[])
      .then(data => data);
  }

  getUsersWithOrdersSmall(): any {
    return this.http.get<any>('assets/demo/data/products-orders-small.json')
      .toPromise()
      .then(res => res.data as CUser[])
      .then(data => data);
  }

  getCountries(): any {
    return this.http.get<any>('assets/demo/data/countries.json')
      .toPromise()
      .then(res => res.data as any[])
      .then(data => data);
  }
}
