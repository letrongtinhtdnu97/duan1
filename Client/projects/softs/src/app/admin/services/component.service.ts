import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const baseUrl = `http://127.0.0.1:7101/ServiceQTHT-QTHT-context-root/resources/ServiceQTHT`;
const option = {
    'Content-Type': 'application/json'
};
@Injectable({
  providedIn: 'root'
})
export class ComponentService {

  constructor(private http: HttpClient) { }

  getAllSupporterToComponent(): any {
    return this.http.get<any>(`${baseUrl}/getAllSupporterToComponent`);
  }
  getAllComponent(): any {
    return this.http.get<any>(`${baseUrl}/getAllComponent`);
  }
  insert(data: string): any {
    return this.http.post<any>(`${baseUrl}/insertComponent`, data, { headers: option });
  }
  update(data: string): any {
      return this.http.post<any>(`${baseUrl}/updateComponent`, data, { headers: option });
  }
  delete(data: string): any {
      return this.http.post<any>(`${baseUrl}/deleteComponent`, data, { headers: option });
  }
}
