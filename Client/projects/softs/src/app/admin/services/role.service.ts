import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Group } from '../models/group.model';
import { Role } from '../models/role.model';
const baseUrl = `http://127.0.0.1:7101/ServiceQTHT-QTHT-context-root/resources/ServiceQTHT`;
const option = {
    'Content-Type': 'application/json'
};
@Injectable()
export class RoleService {

  constructor(private http: HttpClient) { }

  getGroupsSmall(): any {
    return this.http.get<any>('assets/demo/data/products-small.json')
      .toPromise()
      .then(res => res.data as Group[])
      .then(data => data);
  }
  getAllRoles(): any {
      return this.http.get<any>(`${baseUrl}/getAllRole`)
        .toPromise()
        .then(res => res as Role[])
        .then(data => data)
        .catch(e => {
            console.log(e);
            return []
        })
  }
  insert(body: string): any {
      return this.http.post<any>(`${baseUrl}/insertRole`,body,{headers : option})
      .toPromise()
      .then(res => res)
      .catch(e => {
          console.log(e)
          return null;
      });
  }
  delete(body: string): any {
    return this.http.post<any>(`${baseUrl}/deleteRole`,body,{headers : option})
      .toPromise()
      .then(res => res)
      .catch(e => {
        console.log(e)
        return null;
    });
  }
  deleteMulti(body: string):any {
    return this.http.post<any>(`${baseUrl}/deleteMultiRole`,body,{headers : option})
      .toPromise()
      .then(res => res)
      .catch(e => {
        console.log(e)
        return null;
    });
  }
  update(body: string): any {
    return this.http.post<any>(`${baseUrl}/updateRole`,body,{headers : option})
    .toPromise()
    .then(res => res)
    .catch(e => {
      console.log(e)
      return null;
    });
  }
  getGroups(): any {
    return this.http.get<any>('assets/demo/data/products.json')
      .toPromise()
      .then(res => res.data as Group[])
      .then(data => data);
  }

  getGroupsMixed(): any {
    return this.http.get<any>('assets/demo/data/products-mixed.json')
      .toPromise()
      .then(res => res.data as Group[])
      .then(data => data);
  }

  getGroupsWithOrdersSmall(): any {
    return this.http.get<any>('assets/demo/data/products-orders-small.json')
      .toPromise()
      .then(res => res.data as Group[])
      .then(data => data);
  }
}
