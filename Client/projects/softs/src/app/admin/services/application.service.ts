import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const baseUrl = `http://127.0.0.1:7101/ServiceQTHT-QTHT-context-root/resources/ServiceQTHT`;
const option = {
    'Content-Type': 'application/json'
};
@Injectable()
export class ApplicationService {

  constructor(private http: HttpClient) { }
  getAllFile(): any {
    return this.http.get<any>(`${baseUrl}/getAllFile`);
  }
  getAllAppName(): any {
    return this.http.get<any>(`${baseUrl}/getAllAppName`);
  }

  getAllAppCategory(): any {
    return this.http.get<any>(`${baseUrl}/getAllAppCategory`);
  }
  getAllAppType(): any {
    return this.http.get<any>(`${baseUrl}/getAllAppType`);
  }
  getAppforGrantAccess(param: any): any {
    return this.http.post(`${baseUrl}/getAppforGrantAccess`, param, {
      headers: {
        'Accept': 'application/json',
      }
    });
  }
  insertApplist(data: FormData): any {
    return this.http.post<any>(`${baseUrl}/insertApplist`, data, {
      headers: {
        'Accept': 'application/json',
      }
    });
  }
  insertAppListNoImage(data: any): any {
    return this.http.post<any>(`${baseUrl}/insertAppListNoImage`, data, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
  updateApplist(data: FormData): any {
    return this.http.post<any>(`${baseUrl}/updateApplist`, data, {
      headers: {
        'Accept': 'application/json',
      }
    });
  }
  deleteAppList(data: any): any {
    return this.http.post<any>(`${baseUrl}/deleteMutipleAppList`, data, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }
}
