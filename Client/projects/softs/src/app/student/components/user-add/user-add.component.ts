import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { MessageService } from 'primeng/api';
import { User } from '../../models/User';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-add',
  templateUrl: './user-add.component.html',
  styleUrls: ['./user-add.component.css']
})
export class UserAddComponent implements OnInit {
  submited: boolean;
  addDialog: boolean;
  user: User;
  sex: any;
  selectSex: any;
  typeUser: any;
  selectTypeUser: any;
  date: Date
  @Input() addChildren: any;
  @Output() addEvent = new EventEmitter<any>();
  constructor(
    private userService: UserService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.addDialog = false
    this.sex = [
      {value: 1, label: 'Nam'},
      {value: 0, label: 'Nữ'}
    ];
    this.typeUser = [
      {value: 0, label: 'Sinh viên'},
      {value: 1, label: 'Giáo viên'},
      {value: 2, label: 'Ban giám hiệu'},
      {value: 3, label: 'Admin'},
    ]
    this.selectTypeUser = 0;
    this.selectSex = 1;
  }

  open():void {
    this.user = new User();
    this.submited  = false
    this.addDialog = true
  }
  hide(): void {
    this.submited  = false
    this.addDialog = false
  }
  save():void {
    this.submited = true
    this.user = {
      ...this.user,
      sex: this.selectSex,
      type: this.selectTypeUser
    }
    
    this.userService.created(this.user)
      .subscribe(
        (data: any) => {
          this.submited = false;
          console.log(data)
          if(data.code === 1) {
            this.addEvent.emit(1);
            this.hide()
            this.messageService.add({ severity: 'success', summary: 'Thông báo', detail: 'Tạo tài khoản thành công', life: 3000 });
            return
          }
          if(data.code === 2) {            
            return this.messageService.add({ severity: 'info', summary: 'Thông báo', detail: 'Tài khoản đã tồn tại trong hệ thống', life: 3000 });
          }
          return this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Tạo tại khoản không thành công', life: 3000 });
        },
        (err: any) => {
          return this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Tạo tại khoản không thành công', life: 3000 });
        }
      );
    
  }

}
