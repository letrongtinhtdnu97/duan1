import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../demo/service/productservice'
import { Product } from '../../../demo/domain/product'
import { UserService } from '../../services/user.service';
import { User } from '../../models/User';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { UserAddComponent } from '../user-add/user-add.component';
import { UserEditComponent } from '../user-edit/user-edit.component';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
  providers: [MessageService,ConfirmationService]
})
export class UserComponent implements OnInit {
  products: Product[];
  selectedProducts: Product[];
  users: User[];
  selectUsers: User[];
  selectUser: User;
  submited: boolean;
  cols: any;

  @ViewChild('userAdd') addUser: UserAddComponent;
  @ViewChild('userEdit') editUser: UserEditComponent;
  constructor(
    private productService: ProductService,
    private userService: UserService,
    private breadcrumbService: BreadcrumbService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {
    this.breadcrumbService.setItems([
      { label: 'User Management' },
      { label: 'User list' }
    ]);
  }

  ngOnInit(): void {
    this.submited = false;
    this.cols = [
      { field: 'fullname', header: 'Full Name' },
      { field: 'cmnd', header: 'CMND' },
      { field: 'dob', header: 'DOB' },
      { field: 'sex', header: 'Sex' },
      { field: 'email', header: 'Email' },
      { field: 'phone', header: 'Phone' },
      { field: 'type', header: 'TYPE' }
    ];
    this.loadData();
  }
  openNew():void {
    this.addUser.open();
  }
  loadType(id: number): String {
    switch (id) {
      case 0:
        return 'Sinh viên'
      case 1:
        return 'Giáo viên'
      case 2: 
        return 'Ban giám hiệu';
      case 3: 
        return 'Admin'
      default:
        break;
    }
  }
  openEdit(user: User):void {
    this.editUser.user = user;
    this.editUser.date = new Date(user.dob);
    
    this.editUser.open()
  }
  loadData(): void {
    this.userService.getAll().subscribe(
      (data: any) => {
        if(data.code === 1) {
          this.users = data.users;
          return;
        }
        this.messageService.add({ severity: 'error', summary: 'Thong bao', detail: 'Khong co du lieu', life: 3000 });
        return;
      },
      (err: any) => {
        this.messageService.add({ severity: 'error', summary: 'Thong bao', detail: 'Khong co du lieu', life: 3000 });
      }
    )
  }
  
  deleteSelected():void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected user?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.handleDelete();
      }
    });
  }
  handleDelete():void {
    const id = this.selectUsers[0]._id
    this.userService.deleted(id)
      .subscribe(
        (data) => {
          console.log(data)
          this.selectUsers = null;
          this.loadData();
          if(data.code === 1) {
            this.messageService.add({ severity: 'success', summary: 'Thong bao', detail: 'Xoa thanh cong', life: 3000 });
            return;
          }
        }
      );
    
  }

}
