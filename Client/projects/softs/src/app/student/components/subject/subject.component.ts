
import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../demo/service/productservice'
import { Product } from '../../../demo/domain/product'
import { UserService } from '../../services/user.service';
import { User } from '../../models/User';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { UserAddComponent } from '../user-add/user-add.component';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { Course } from '../../models/Course';
import { SubjectService } from '../../services/subject.service';
import { SubjectAddComponent } from '../subject-add/subject-add.component';
import { SubjectEditComponent } from '../subject-edit/subject-edit.component';
import { SubjectViewComponent } from '../subject-view/subject-view.component';
@Component({
  selector: 'app-subject',
  templateUrl: './subject.component.html',
  styleUrls: ['./subject.component.css'],
  providers: [MessageService,ConfirmationService]
})
export class SubjectComponent implements OnInit {
  users: User[];
  selectUsers: User[];
  selectUser: User;
  submited: boolean;
  cols: any;

  courses: Course[];
  selectCourses: Course[];


  @ViewChild('subjectAdd') addSubject: SubjectAddComponent;
  @ViewChild('subjectEdit') editSubject: SubjectEditComponent;
  @ViewChild('subjectUser') userSubject: SubjectViewComponent;
  
  constructor(
    private courseService: SubjectService,
    private userService: UserService,
    private breadcrumbService: BreadcrumbService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {
    this.breadcrumbService.setItems([
      { label: 'Khóa học' },
      { label: 'Môn học' }
    ]);
  }

  ngOnInit(): void {
    this.submited = false;
    this.cols = [
      { field: 'fullname', header: 'Full Name' },
      { field: 'cmnd', header: 'CMND' },
      { field: 'dob', header: 'DOB' },
      { field: 'sex', header: 'Sex' },
      { field: 'email', header: 'Email' },
      { field: 'phone', header: 'Phone' },
      { field: 'type', header: 'TYPE' }
    ];
    this.loadData();
  }
  openNew():void {
    this.addSubject.open();
  }
  loadType(id: number): String {
    switch (id) {
      case 0:
        return 'Mới tạo'
      case 1:
        return 'Cho đăng kí'
      case 2: 
        return 'Kết thúc đăng kí';
      case 3: 
        return 'Đang học'
        case 4: 
        return 'Đã kết thúc'
      default:
        break;
    }
  }
  openEdit(cours: Course):void {
    this.editSubject.course = cours
    this.editSubject.selectTeacher = cours.author._id
    this.editSubject.course.start  = new Date(cours.start)
    this.editSubject.course.end  = new Date(cours.end)
    this.editSubject.selectStatus = cours.type
    this.editSubject.open();
  }
  loadData(): void {
    this.courseService.getAll().subscribe(
      (data: any) => {
        if(data.code === 1) {
          this.courses = data.course;
          return;
        }
        this.messageService.add({ severity: 'error', summary: 'Thong bao', detail: 'Khong co du lieu', life: 3000 });
        return;
      },
      (err: any) => {
        this.messageService.add({ severity: 'error', summary: 'Thong bao', detail: 'Khong co du lieu', life: 3000 });
      }
    )
  }
  
  deleteSelected():void {
    this.confirmationService.confirm({
      message: 'Are you sure you want to delete the selected user?',
      header: 'Confirm',
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.handleDelete();
      }
    });
  }
  handleDelete():void {
    const id = this.selectUsers[0]._id
    this.userService.deleted(id)
      .subscribe(
        (data) => {
          console.log(data)
          this.selectUsers = null;
          this.loadData();
          if(data.code === 1) {
            this.messageService.add({ severity: 'success', summary: 'Thong bao', detail: 'Xoa thanh cong', life: 3000 });
            return;
          }
        }
      );
    
  }
  addUser(course: Course):void {
    this.userSubject.selectUsers = course.students
    console.log( course.students)
    this.userSubject.open()
  }

}
