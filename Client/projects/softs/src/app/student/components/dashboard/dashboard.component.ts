import {Component, OnInit} from '@angular/core';


import {SelectItem, MenuItem} from 'primeng/api';


import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { ProductService } from '../../../demo/service/productservice';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { EventService } from '../../../demo/service/eventservice';
import { Product } from '../../../demo/domain/product';

@Component({
    templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

    cities: SelectItem[];

    products: Product[];

    chartData: any;

    events: any[];

    selectedCity: any;

    items: MenuItem[];

    fullcalendarOptions: any;

    constructor(private productService: ProductService, private eventService: EventService,
                private breadcrumbService: BreadcrumbService) {
        this.breadcrumbService.setItems([
            {label: ''}
        ]);
    }

    ngOnInit() {
        this.eventService.getEvents().then(events => {this.events = events; });

        this.fullcalendarOptions = {
            plugins: [ dayGridPlugin, timeGridPlugin, interactionPlugin ],
            defaultDate: '2021-05-23',
            header: {
                left: 'prev,next, today',
                center: 'title',
                right: 'dayGridMonth,timeGridWeek,timeGridDay'
            }
        };
    }
}
