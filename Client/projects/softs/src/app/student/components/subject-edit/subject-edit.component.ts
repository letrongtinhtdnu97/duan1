import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { Course } from '../../models/Course';
import { User } from '../../models/User';
import { SubjectService } from '../../services/subject.service';
import { UserService } from '../../services/user.service';
import { MessageService } from 'primeng/api';
@Component({
  selector: 'app-subject-edit',
  templateUrl: './subject-edit.component.html',
  styleUrls: ['./subject-edit.component.css']
})
export class SubjectEditComponent implements OnInit {

  submited: boolean;
  addDialog: boolean;
  course: Course;
  author: any;
  status: any;
  selectStatus: any;
  teachers: any[];
  selectTeacher: any;
  @Output() editEvent = new EventEmitter<any>();
  constructor(
    private userService: UserService,
    private subjectService: SubjectService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.addDialog = false;
    this.course = new Course();
    this.status = [
      {value: 0, label: 'Mới tạo'},
      {value: 1, label: 'Chờ đăng kí'},
      {value: 2, label: 'Kết thúc đăng kí'},
      {value: 3, label: 'Đang học'},
      {value: 4, label: 'Đã kết thúc'},
    ]
    this.userService.getTeacher()
      .subscribe(
        (data:any) => {
          console.log(data);
          if(data.code === 1) {
            this.teachers = []
            data.users.map((item: User) => {
              this.teachers.push({value: item._id, label: item.fullname})
            })
          }
        }
      );
    
  }
  open(): void {
    this.addDialog = true;
  }
  hide():void {
    this.addDialog = false
  }
  save():void {
    const param = {
      ...this.course,
      type: 0,
      author: this.selectTeacher
    }
    this.subjectService.updated(param)
      .subscribe(
        (data: any) => {
          console.log(data);
          if(data.code === 1) {
            this.editEvent.emit(1)
            this.hide();
            this.messageService.add({ severity: 'success', summary: 'Thông báo', detail: 'Tạo môn học thành công', life: 3000 });
            return;
          }
          this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Tạo môn học không thành công', life: 3000 });
        }
      )
  }
}
