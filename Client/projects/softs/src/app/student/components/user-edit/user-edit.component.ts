import { Component, OnInit, Input, Output,EventEmitter } from '@angular/core';
import { MessageService } from 'primeng/api';
import { User } from '../../models/User';
import { UserService } from '../../services/user.service';
@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  user: User
  submited: boolean;
  editDialog: boolean;
  sex: any;
  typeUser: any;
  selectTypeUser: number;
  selectSex: any;
  date: Date;
  @Input() addChildren: any;
  @Output() editEvent = new EventEmitter<any>();
  constructor(
    private userService: UserService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.submited = false
    this.editDialog = false
    this.user = new User();
    this.sex = [
      {value: 1, label: 'Nam'},
      {value: 0, label: 'Nữ'}
    ];
    this.typeUser = [
      {value: 0, label: 'Sinh viên'},
      {value: 1, label: 'Giáo viên'},
      {value: 2, label: 'Ban giám hiệu'},
      {value: 3, label: 'Admin'},
    ]
    
    
    
    
  }
  hide():void {
    this.submited = false
    this.editDialog = false
  }
  open():void {
    this.submited = false
    this.editDialog = true
    this.date = new Date(this.user?.dob)
    this.selectTypeUser = this.user?.type;
    
    this.selectSex = this.user?.sex ? 1 : 0;
    
  }
  save():void {
    this.submited = true
    const param = {
      ...this.user,
      sex: this.selectSex,
      type: this.selectTypeUser,
      id: this.user._id
    }
    this.userService.updated(param)
      .subscribe(
        (data) => {
          console.log('data',data)
          if(data.code === 1) {
            this.hide();
            this.editEvent.emit(1)
            return this.messageService.add({ severity: 'success', summary: 'Thông báo', detail: 'Chinh sua thành công', life: 3000 });
          }
          this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Chinh sua khong thành công', life: 3000 });
        },
        (err: any) => {
          this.messageService.add({ severity: 'error', summary: 'Thông báo', detail: 'Chinh sua khong thành công', life: 3000 });
        }
      );
    
  }

}
