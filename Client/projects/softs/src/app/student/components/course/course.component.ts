import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductService } from '../../../demo/service/productservice'
import { Product } from '../../../demo/domain/product'
import { UserService } from '../../services/user.service';
import { User } from '../../models/User';
import { ConfirmationService } from 'primeng/api';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from '../../../core/services/breadcrumb.service';
import { UserAddComponent } from '../user-add/user-add.component';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { SubjectService } from '../../services/subject.service';
import { Course } from '../../models/Course';
@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css'],
  providers: [MessageService,ConfirmationService]
})
export class CourseComponent implements OnInit {

  products: Product[];
  selectedProducts: Product[];
  users: User[];
  selectUsers: User[];
  selectUser: User;
  submited: boolean;
  cols: any;
  querys: any[];
  selectQuery: any;

  @ViewChild('userAdd') addUser: UserAddComponent;
  @ViewChild('userEdit') editUser: UserEditComponent;
  constructor(
    private userService: UserService,
    private breadcrumbService: BreadcrumbService,
    private courseService: SubjectService,
    private messageService: MessageService,
    private confirmationService: ConfirmationService
  ) {
    this.breadcrumbService.setItems([
      { label: 'User Management' },
      { label: 'User list' }
    ]);
  }

  ngOnInit(): void {
    this.submited = false;
    this.cols = [
      { field: 'fullname', header: 'Full Name' },
      { field: 'cmnd', header: 'CMND' },
      { field: 'dob', header: 'DOB' },
      { field: 'sex', header: 'Sex' },
      { field: 'email', header: 'Email' },
      { field: 'phone', header: 'Phone' },
      { field: 'type', header: 'TYPE' }
    ];
    this.loadData();
  }
  openNew():void {
    this.addUser.open();
  }
  loadType(id: number): String {
    switch (id) {
      case 0:
        return 'Sinh viên'
      case 1:
        return 'Giáo viên'
      case 2: 
        return 'Ban giám hiệu';
      case 3: 
        return 'Admin'
      default:
        break;
    }
  }
  openEdit(user: User):void {
    this.editUser.user = user;
    this.editUser.date = new Date(user.dob);
    
    this.editUser.open()
  }
  loadSearch():void {
    
  }
  loadData(): void {
    this.courseService.getAll()
      .subscribe(
        (data: any) => {
          if(data.code === 1) {
            this.querys = [];
            data.course.map((item: Course) => {
              if(item.students.length > 0) {
                this.querys.push({value: item._id, label: item.title + ' - ' + item.author.fullname})
              }
            })
          }
        }
      );
    
  }
  
  


}
