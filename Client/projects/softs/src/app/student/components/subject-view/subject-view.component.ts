import { Component, OnInit } from '@angular/core';
import { User } from '../../models/User';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-subject-view',
  templateUrl: './subject-view.component.html',
  styleUrls: ['./subject-view.component.css']
})
export class SubjectViewComponent implements OnInit {
  addDialog: boolean;
  users: any[];
  selectUsers: any[];
  constructor(
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.addDialog = false
    this.selectUsers = [];
    this.userService.getStudents()
      .subscribe(
        (data: any) => {

          if(data.code === 1) {
            this.users = [];
            data.users.map((item: User) => {
              this.users.push({value: item._id, label: item.fullname + '-' + item.cmnd})
            })
          }
        }
      );
    
    
    
  }
  open():void {
    this.addDialog = true
  } 
  hide():void {
    this.addDialog = false
  }
  save():void {
    console.log(this.selectUsers)
  }

}
