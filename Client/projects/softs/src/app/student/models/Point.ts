import { User } from "./User";

export class Point {
    name?: String;
    _id?: String;
    students?: User; 
    check1?: number;
    check2?: number;
    check3?: number;
    mid_check?: number;
    end_check?: number;
    exam?: number;
    point10?: number;
    point4?: number;
    classification?: String;
}