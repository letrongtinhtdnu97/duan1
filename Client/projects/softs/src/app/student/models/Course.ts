import { User } from "./User";

export class Course {
    status?: boolean;
    students?: any;
    _id?: String;
    author?: User;
    title?: String;
    content?: String;
    link?: String;
    count?: number;
    type?: number;
    start?: Date;
    end?: Date;
    created?: Date;
    updated?: Date
}