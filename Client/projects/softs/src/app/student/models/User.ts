export class User {
    fullname?: string;
    cmnd?: string;
    address?: string;
    dob?: Date;
    sex?: boolean;
    email?: string;
    phone?: string;
    password?: string;
    avatar?: string;
    type?: number;
    updated?: Date;
    created?: Date;
    _id: string
}

