import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const baseUrl = `http://localhost:3003/course/`;
const option = {
    'Content-Type': 'application/json'
};
@Injectable()
export class SubjectService {

  constructor(private http: HttpClient) { }
  getAll(): any {
    return this.http.get<any>(baseUrl);
  }
  getById(param: string): any {
      return this.http.get<any>(baseUrl + `${param}`)
  }
  created(param: any): any {
    return this.http.post(baseUrl + 'created', param)
  }
  updated(param: any): any {
    return this.http.post(baseUrl + 'updated', param)
  }
  deleted(param: any): any {
    return this.http.delete(baseUrl + `${param}`)
  }
  
  
}
