import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class AuthService {

  constructor(private http: HttpClient) { }
  getStore(): any {
    localStorage.getItem('user')
  }
  setStore(user): any {
    localStorage.setItem('user',JSON.stringify(user))
  }
  
}
