const Subject = require('../models/Subject');

const func_subjectController_GetAll = async(req,res,next) => {
    try {
        const sub = await Subject.find().sort({created:-1})
        return res.status(200).json({
            code: 1,
            subjects: sub
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            subjects: []
        })
    }
}
const func_subjectController_GetById = async(req, res, next) => {
    const {id} = req.params;
    try {
        const subject = await Subject.findOne({_id: id});
        return res.status(200).json({
            code: 1,
            subject,
            message: 'Success'
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: "Server error",
            subject: []
        })
    }
}
const func_subjectController_Created = async(req,res,next) => {
    try {
        
    } catch (error) {
        
    }
}
const func_subjectController_Updated = async(req,res,next) => {
    try {
        
    } catch (error) {
        
    }
}
const func_subjectController_Deleted = async(req,res,next) => {
    try {
        
    } catch (error) {
        
    }
}
module.exports = {
    GetAll: func_subjectController_GetAll,
    GetById: func_subjectController_GetById,
    Created: func_subjectController_Created,
    Updated: func_subjectController_Updated,
    Deleted: func_subjectController_Deleted
}