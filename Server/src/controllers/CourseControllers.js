const Course  = require('../models/Course');
const User = require('../models/User');

const func_courseController_GetAll = async(req,res,next) => {
    try {
        const course = await Course.find().populate('author')
        .sort({created:-1});
        return res.status(200).json({
            code: 1,
            course,
            message: 'Success'
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: "Server error",
            course: []
        })
    }
}

const func_courseController_GetById = async(req, res, next) => {
    const {id} = req.params;
    try {
        const course = await Course.findOne({_id: id});
        return res.status(200).json({
            code: 1,
            course,
            message: 'Success'
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: "Server error",
            course: []
        })
    }
}

const func_courseController_Created = async(req,res,next)=> {
    console.log(req.body)
    if(!req.body.author) {
        return res.status(200).json({
            code: 2
        })
    }
    try {
        const newCourse = new Course(req.body)
        await newCourse.save()
        return res.status(200).json({
            code: 1,
            message: 'Success'
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: 'Success'
        })
    }
}
const func_courseController_Updated = async(req,res,next)=> {
    try {
        
        await Course.findOneAndUpdate({_id:req.body._id},req.body,{upsert:true})
        return res.status(200).json({
            code: 1,
            message: 'Success'
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: 'Success'
        })
    }
}
const func_courseController_Deleted = async(req,res,next)=> {
    try {
        
        await Course.findOneAndDelete({_id:req.body._id})
        return res.status(200).json({
            code: 1,
            message: 'Success'
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: 'Success'
        })
    }
}
const func_courseController_AddUser = async(req,res,next) => {
    const {students} = req.body;
    try {
        
        const user = await Course.findOne({_id: req.body._id})
        
        const arr = user.students.concat(students)
        
        await Course.findOneAndUpdate({_id: req.body._id},{students: arr},{upsert: true})
        
        return res.status(200).json({
            code: 1
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            code: 0
        })
    }
}
const func_courseController_DeleteUser = async(req,res,next) => {
    const {_id,id} = req.body;
    try {
        const user = await Course.findOne({_id: _id})
        const arr = user.students.filter(item => item != id)
        console.log(arr)
        await Course.findOneAndUpdate({_id: _id},{students: arr},{upsert: true})
        return res.status(200).json({
            code: 1
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            code: 0
        }) 
    }
}
module.exports = {
    GetAll: func_courseController_GetAll,
    GetById: func_courseController_GetById,
    Created: func_courseController_Created,
    Updated: func_courseController_Updated,
    Deleted: func_courseController_Deleted,
    AddUser: func_courseController_AddUser,
    DeleteUser: func_courseController_DeleteUser,
}