const Point = require("../models/Point");

const func_pointController_GetAll = async(req,res,next) => {
    const {id} = req.body
    try {
        const points = await Point.findOne({course:id})
        .populate('course','title')
        .populate('user','fullname')
        .sort({created:-1});
        return res.status(200).json({
            code: 1,
            points,
            message: 'Success'
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: "Server error",
            points: []
        })
    }
}
const func_pointController_Created = async(req,res,next) => {
    try {
        const savePoint = new Point(req.body)
        await savePoint.save()
        return res.status(200).json({
            code: 1
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            code: 0
        })
    }
}
module.exports = {
    GetAll: func_pointController_GetAll,
    Created: func_pointController_Created
}