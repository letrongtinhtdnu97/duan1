
const User = require('../models/User');

const func_userController_GetAll = async(req,res,next) => {
    try {
        const users = await User.find().sort({created:-1});
        return res.status(200).json({
            code: 1,
            users,
            message: 'Success'
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: "Server error",
            users: []
        })
    }
}
const func_userController_GetById = async(req,res,next) => {
    const { id } = req.params 
    try {
        const user = await User.findOne({_id: id});
        if(user) {
            return res.status(200).json({
                code: 1,
                message: "success",
                user
            }) 
        }
        return res.status(200).json({
            code: 2,
            message: "No data",
            user
        }) 
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: "Server error",
            user: {}
        })
    }
}
const func_userController_createUser = async(req,res,next) => {
    const {fullname,cmnd,address, dob, sex,email, phone, type} = req.body
    try {
        const newUser = {
            fullname,
            cmnd,
            address,
            dob: dob,
            sex,
            email,
            phone,
            type
        };
        const findUser = await User.findOne({cmnd})
        
        if(findUser) {
            return res.status(200).json({
                code: 2,
                message: "User ton tai"
            })
        }
        const saveUser = new User(newUser)
        await saveUser.save()

        return res.status(200).json({
            code: 1,
            message: "Success"
        })
    } catch (error) {
        console.log(error)
        return res.status(500).json({
            code: 0,
            message: "Server error"
        })
    }
}

const func_userController_updateUser = async(req,res,next) => {
    try {
        const {id,fullname,cmnd,address, dob, sex,email, phone} = req.body
        const findUser = await User.findOne({_id: id})
        console.log(findUser)
        if(!findUser) {
            return res.status(200).json({
                code: 2,
                message: "User khong ton tai"
            })
        }
        const updateUser = {
            fullname,
            cmnd,
            address,
            dob: dob,
            sex,
            email,
            phone,
            updated_at: new Date
        }
        await User.findOneAndUpdate({_id:id},updateUser,{upsert:true})
        return res.status(200).json({
            code: 1,
            message: 'Success'
        })
    } catch (error) {
        return res.status(500).json({
            code: 0,
            message: "Server error"
        })
    }
}

const func_userController_deleteUser = async(req ,res ,next) => {
    const {id}  = req.params
    try {
        const user = await User.findOne({_id:id})
        if(!user) {
            return res.status(200).json({
                code: 2,
                message: 'User found'
            })
        }
        await User.findOneAndDelete({_id: id})
        return res.status(200).json({
            code: 1,
            message: 'Success'
        })
    } catch (error) {
        res.json(500).json({
            code: 0,
            message: "Failure"
        })
    }
}
const func_userController_Teacher = async(req,res,next) => {
    try {
        const users = await User.find({type: 1}).sort({created:-1});
        return res.status(200).json({
            code: 1,
            users,
            message: 'Success'
        })
    } catch (error) {
        res.json(500).json({
            code: 0,
            message: "Failure"
        })
    }
}

const func_userController_Student = async(req,res,next) => {
    try {
        const users = await User.find().sort({created:-1});
        return res.status(200).json({
            code: 1,
            users,
            message: 'Success'
        })
    } catch (error) {
        res.json(500).json({
            code: 0,
            message: "Failure"
        })
    }
}
module.exports = {
    GetAll: func_userController_GetAll,
    GetById: func_userController_GetById,
    Created: func_userController_createUser,
    Updated: func_userController_updateUser,
    Deleted: func_userController_deleteUser,
    GetTeacher: func_userController_Teacher,
    GetStudent: func_userController_Student
}