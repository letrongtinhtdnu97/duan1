const express = require('express')
const path = require('path')
const mongoose = require('mongoose');
const cors = require('cors');
//route
const userRoute = require('./routes/users');
const courseRoute = require('./routes/course');
const pointRoute = require('./routes/points');
const app = express()
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(express.static((path.join(__dirname, 'public'))))
app.use(cors())
//setup mongo

mongoose.
connect('mongodb://localhost:27017/dbcourse', {
    useNewUrlParser: true, 
    useUnifiedTopology: true,
    useFindAndModify:false
})
.then(() => {
  console.log("connect mongodb success")
}).catch((e)=>{
  console.log("Connect mongodb failure",e)
});
//end setup mongo

app.use('/users',userRoute)
app.use('/course',courseRoute)
app.use('/points',pointRoute);
app.use('/', async(req,res,next) => {
    return res.json({
        message: 'Wellcome api'
    })
})
app.use(function (req, res, next) {
    next(createError(404));
});
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    // res.render('error')
    res.json({ error: err })
});

module.exports = app