const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const User = new Schema({
  fullname: {type: String},
  cmnd: {type: String},
  address: {type: String},
  dob: {type: Date},
  sex: {type: Boolean, default: true},
  email: {type: String},
  phone: {type: String},
  password: {type: String},
  avatar: {type: String},
  type: {type: Number, default: 0},
  updated: { type: Date, default: new Date},
  created:  { type: Date, default: new Date }
});

module.exports = mongoose.model('Users',User);