const mongoose = require('mongoose');
const User = require('./User');
const Schema = mongoose.Schema;


const Subjects = new Schema({
  name: {type: String},
  status: {type: Boolean},
  credits: {type: Number},
  yearSchool: {type: Number},
  updated: { type: Date, default: new Date},
  created:  { type: Date, default: new Date }
});

module.exports = mongoose.model('Subjects',Subjects);