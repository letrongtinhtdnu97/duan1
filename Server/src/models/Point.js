const mongoose = require('mongoose');
const User = require('./User');
const Schema = mongoose.Schema;


const Point = new Schema({
  name: {type: String},
  course: {type: Schema.Types.ObjectId, ref: 'Courses' },
  user: {type: Schema.Types.ObjectId, ref: 'Users' },
  check1: {type: Number},
  check2: {type: Number},
  check3: {type: Number},
  mid_check: {type: Number},
  end_check: {type: Number},
  exam: {type: Number},
  point10: {type: Number},
  point4: {type: Number},
  classification: {type: String},
  updated: { type: Date, default: new Date},
  created:  { type: Date, default: new Date }
});

module.exports = mongoose.model('Points',Point);