const mongoose = require('mongoose');
const User = require('./User');
const Schema = mongoose.Schema;


const Course = new Schema({
  title: {type: String},
  author: { type: Schema.Types.ObjectId, ref: 'Users' },
  content: {type: String},
  body: {type: String},
  link: {type: String},
  count: {type: Number},
  type: {type: Number, default: 0},
  status: {type: Boolean, default: true},
  start: {type: Date},
  end: {type: Date},
  students: [{type: Schema.Types.ObjectId, ref: 'Users'}],
  updated: { type: Date, default: new Date},
  created:  { type: Date, default: new Date }
});

module.exports = mongoose.model('Courses',Course);