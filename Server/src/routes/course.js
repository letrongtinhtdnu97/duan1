const express = require('express');
const router = express.Router();
const courseController = require('../controllers/CourseControllers');
router.get('/',courseController.GetAll);
router.get('/:id', courseController.GetById)
router.delete('/:id',courseController.Deleted)
router.post('/created', courseController.Created)
router.post('/updated', courseController.Updated);
router.post('/add-user',courseController.AddUser)
router.post('/delete-user',courseController.DeleteUser)
module.exports = router;