const express = require('express');
const router = express.Router();
const subjectController = require('../controllers/SubjectControllers');
router.get('/',subjectController.GetAll);
router.get('/:id', subjectController.GetById)
router.delete('/:id',subjectController.Deleted)
router.post('/created', subjectController.Created)
router.post('/updated', subjectController.Updated);

module.exports = router;