const express = require('express');
const router = express.Router();
const UserController = require('../controllers/UserControllers');
router.get('/', UserController.GetAll)
router.get('/:id', UserController.GetById)
router.delete('/:id', UserController.Deleted)
router.post('/created', UserController.Created)
router.post('/updated', UserController.Updated)
router.post('/user-teacher', UserController.GetTeacher)
router.post('/user-students', UserController.GetStudent)
module.exports = router;